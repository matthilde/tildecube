 __   __ __     __                    __          
|  |_|__|  |.--|  |.-----.----.--.--.|  |--.-----.
|   _|  |  ||  _  ||  -__|  __|  |  ||  _  |  -__| Voxel sandbox game
|____|__|__||_____||_____|____|_____||_____|_____| by matthilde

Webpage: https://matthil.de/tildecube.html

Tildecube is a voxel sandbox game heavily inspired by the very early version of
Minecraft: Minecraft "Classic" 0.30. The game features embedded Groovy
scripting, compatibility with ClassicWorld format, and more.

STAGE OF DEVELOPMENT
--------------------

The game is playable! Although kind of boring due to a lack of very cool
features, please check the webpage as I list all the game features in there!
There's no public builds for now, but you can compile it yourself :). It uses
LWJGL 3, Groovy and an NBT library, with Maven as building tool.

LICENSING
---------

My codebase (that is, Java source code and GLSL shader sources) is licensed
under the Mozilla Public License 2.0.

As of assets,
 * terrain.png and gui.png uses the Jolicraft texture pack.
   Please check http://jolicraft.com/, this is a beautiful texture pack.
 * font.png is a public domain font found in opengameart.org
 * rest is under Public Domain
