#!/usr/bin/env sh
# Quick and dirty script for building jaaj cooc

compile () {
    mvn compile
}

# Compiles JAR
jar () {
    mvn clean compile assembly:single
}

run () {
    # java -jar target/voxelgame-jar-*.jar
    mvn exec:java
}

main () {
    case "$1" in
        run) compile && run ;;
        compile) compile ;;
        jar) jar ;;
        *) echo "Unknown command" && exit 1 ;;
    esac
}

main "$@"
