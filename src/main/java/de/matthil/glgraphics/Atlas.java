package de.matthil.glgraphics;

public class Atlas {
	private Texture tex;
	private float tileWidth, tileHeight;
	private int tileWidthPx, tileHeightPx;
	private int width, height;
	
	private void init(Texture tex) {
		this.tileWidth = 1;
		this.tileHeight = 1;
		this.width = tex.width();
		this.height = tex.height();
		
		this.tileWidthPx = tex.width();
		this.tileHeightPx = tex.height();
	}
	public Atlas(String filename) {
		this.tex = new Texture(filename);
		init(tex);
	}
	public Atlas(Texture tex) {
		this.tex = tex;
		init(tex);
	}
	
	//// Set properties
	public Atlas subdivide(float x, float y) {
		this.tileWidth = 1 / x;
		this.tileHeight = 1 / y;
		this.tileWidthPx = (int)(width / x);
		this.tileHeightPx = (int)(height / y);
		
		this.width = (int)(tex.width() / x);
		this.height = (int)(tex.height() / y);
		
		return this;
	}
	public Atlas setTileSize(int width, int height) {
		this.tileWidth = (float)width / tex.width();
		this.tileHeight = (float)height / tex.height();
		this.tileWidthPx = width;
		this.tileHeightPx = height;
		
		this.width = tex.width() / width;
		this.height = tex.height() / height;
		
		return this;
	}
	
	public Texture texture() { return tex; }
	
	public void bind() {
		tex.bind();
	}
	public void unbind() {
		tex.unbind();
	}
	public void delete() {
		tex.delete();
	}
	
	public int tileWidth() { return tileWidthPx; }
	public int tileHeight() { return tileHeightPx; }
	
	// first pair is position
	// last pair is size
	public void getUV(float[] uv, int x, int y) {
		float u = tileWidth * (float)x;
		float v = tileHeight * (float)y;
		
		uv[0] = u; uv[1] = v;
		uv[2] = tileWidth; uv[3] = tileHeight;
	}
	public void getUV(float[] uv, int idx) {
		int x = idx % width;
		int y = idx / width;
		getUV(uv, x, y);
	}
	public void getUVfromCoords(float[] uv, float minX, float minY, float maxX, float maxY) {
		uv[0] = minX / width;
		uv[1] = minY / height;
		
	}
	public void addUVtoMesh(Mesh mesh, int x, int y) {
		float u = tileWidth * (float)x;
		float v = tileHeight * (float)y;
		/*
		mesh.addUV(0, 0)
			.addUV(1, 0)
			.addUV(0, 1)
			.addUV(1, 1);
			*/
		
		mesh.addUV(u, v)
			.addUV(u+tileWidth, v)
			.addUV(u, v+tileHeight)
			.addUV(u+tileWidth, v+tileHeight);
	}
	public void addUVtoMesh(Mesh mesh, int idx) {
		int x = idx % width;
		int y = idx / width;
		addUVtoMesh(mesh, x, y);
	}
}
