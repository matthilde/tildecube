package de.matthil.glgraphics;

import org.joml.Vector3f;
import org.joml.Matrix4f;
import org.joml.FrustumIntersection;
import static org.joml.Math.*;

public class Camera {
	private FrustumIntersection frustum;
	private Vector3f position, rotation, direction, tmp, up;
	private Matrix4f mTmp, mat, perspective;
	
	public Camera() {
		frustum  = new FrustumIntersection();
		position = new Vector3f();
		rotation = new Vector3f(0, 90, 0);
		direction = new Vector3f();
		tmp      = new Vector3f();
		mat      = new Matrix4f();
		up       = new Vector3f(0, 1, 0);
		mTmp     = new Matrix4f();
		
		updateDirection();
	}
	
	private void updateDirection() {
		rotation.x = clamp(-89.9f, 89.9f, rotation.x);
		direction = direction.set(
			cos(toRadians(rotation.x())) * cos(toRadians(rotation.y())),
			sin(toRadians(rotation.x())),
			sin(toRadians(rotation.y())) * cos(toRadians(rotation.x()))
		).normalize();
	}
	
	public void setPerspectiveMatrix(Matrix4f perspective) {
		this.perspective = perspective;
	}
	
	public Vector3f direction() { return direction; }
	public Vector3f position() { return position; }
	public float x() { return position.x; }
	public float y() { return position.y; }
	public float z() { return position.z; }
	public float pitch() { return rotation.x; }
	public float yaw() { return rotation.y; }
	
	public void move(float x, float y, float z) {
		position.set(x, y, z);
	}
	public void rotate(float pitch, float yaw) {
		rotation.set(pitch, yaw, 0);
		updateDirection();
	}
	public void moveRelative(float x, float y, float z) {
		position.add(x, y, z);
	}
	public void moveRelative(Vector3f xyz) {
		position.add(xyz);
	}
	public void moveRelative(Vector3f xyz, float multiplier) {
		position.add(xyz.mul(multiplier, tmp));
	}
	public void rotateRelative(float pitch, float yaw) {
		rotation.add(pitch, yaw, 0);
		updateDirection();
	}
	
	public Matrix4f getCameraMatrix() {
		mat = mat.identity();
		return mat.lookAt(position, position.add(direction, tmp), up);
	}
	public Matrix4f getWholeCameraMatrix() {
		mat = getCameraMatrix();
		return perspective.mul(mat, mTmp);
	}
	
	public FrustumIntersection getFrustum() {
		return frustum.set(getWholeCameraMatrix());
	}
}
