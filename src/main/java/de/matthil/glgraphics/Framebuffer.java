package de.matthil.glgraphics;

import static org.lwjgl.opengl.GL33.*;

public class Framebuffer {
	private int fbo, rbo, msfbo;
	private Texture fboTexture;
	private int width, height;
	
	public Framebuffer(int width, int height) {
		this.width = width;
		this.height = height;
		
		init();
	}
	
	public Texture texture() { return fboTexture; }
	
	// That's a tad fucking lot of boilerplate code.
	private void init() {
		msfbo = glGenFramebuffers();
		fbo   = glGenFramebuffers();
		rbo   = glGenRenderbuffers();
		
		glBindFramebuffer(GL_FRAMEBUFFER, msfbo);
		glBindRenderbuffer(GL_RENDERBUFFER, rbo);
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, 0, GL_RGB, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, rbo);
		
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			throw new IllegalStateException("Unable to init framebuffer");
		
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		fboTexture = new Texture(Texture.newTexture(width, height, GL_RGB, null), width, height);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboTexture.id(), 0);
		
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			throw new IllegalStateException("Unable to init framebuffer");
	
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
	public void begin() {
		glBindFramebuffer(GL_FRAMEBUFFER, msfbo);
	}
	
	public void end() {
		glBindFramebuffer(GL_READ_FRAMEBUFFER, msfbo);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
		glBlitFramebuffer(0, 0, width, height,
					      0, 0, width, height,
					      GL_COLOR_BUFFER_BIT, GL_NEAREST);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}
