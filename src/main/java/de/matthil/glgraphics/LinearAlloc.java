package de.matthil.glgraphics;

// import java.nio.FloatBuffer;
import static org.lwjgl.system.MemoryUtil.*;

public class LinearAlloc {
	long buffer;
	int used, size, initialSize;
	
	public LinearAlloc(int size) {
		initialSize = size;
		this.size = size;
		used = 0;
		
		buffer = nmemAlloc(size * 4);
	}
	
	public int size() { return used; }
	public int realSize() { return used * 4; }
	public long buffer() { return buffer; }
	public void free() {
		nmemFree(buffer);
	}
	
	public void rewind() {
		used = 0;
	}
	public void clear() {
		// buffer = nmemRealloc(buffer, initialSize * 4);
		buffer = nmemRealloc(buffer, initialSize * 4);
		size = initialSize;
		used = 0;
	}
	public long allocate(int size) {
		int oldUsed = used;
		used += size;
		while (used >= this.size) {
			this.size *= 2;
			buffer = nmemRealloc(buffer, this.size * 4);
		}
		
		return buffer + oldUsed * 4;
	}
	public void add(float f) {
		memPutFloat(buffer + (used * 4), f);
		
		used++;
		if (used >= size) {
			size *= 2;
			buffer = nmemRealloc(buffer, size * 4);
		}
	}
	public void add(int i) {
		memPutInt(buffer + (used * 4), i);
		
		used++;
		if (used >= size) {
			size *= 2;
			buffer = nmemRealloc(buffer, size * 4);
		}
	}
	public void set(int pos, float f) {
		memPutFloat(buffer + pos * 4, f);
	}
	public void set(int pos, int i)  {
		memPutInt(buffer + pos * 4, i);
	}
	public float getFloat(int pos) {
		return memGetFloat(buffer + pos * 4);
	}
	public int getInt(int pos) {
		return memGetInt(buffer + pos * 4);
	}
}
