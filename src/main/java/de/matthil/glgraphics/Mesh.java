package de.matthil.glgraphics;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL33.*;

public class Mesh {
	private LinearAlloc vertices = new LinearAlloc(64);
	private LinearAlloc normals = new LinearAlloc(64);
	private LinearAlloc uv = new LinearAlloc(64);
	private LinearAlloc luma = new LinearAlloc(64);
	private int vertexArraySize;
	
	private Matrix4f transform = new Matrix4f();
	private Vector3f position = new Vector3f();
	private Vector3f scale = new Vector3f(1, 1, 1);
	// private Vector4f rotation = new Vector4f();
	
	int vao, vbo;
	boolean loaded = false;
	
	public Mesh() {
		vbo = glGenBuffers();
		vao = glGenVertexArrays();
	}
	
	public void setPosition(float x, float y, float z) {
		position.set(x, y, z);
	}
	public void setScale(float scale) {
		this.scale.set(scale, scale, scale);
	}
	public Matrix4f getTransform() {
		return transform.identity().translate(position).scale(scale);
	}
	
	public void delete() {
		vertices.free();
		normals.free();
		uv.free();
		luma.free();
		
		glDeleteBuffers(vbo);
		glDeleteVertexArrays(vao);
	}
	
	public void bind() {
		glBindVertexArray(vao);
	}
	
	public void draw() {
		if (!loaded) return;
		bind();
		glDrawArrays(GL_TRIANGLES, 0, vertexArraySize);
		glBindVertexArray(0);
	}
	
	public void commit() {
		loaded = true;
		int vsize = vertices.realSize(),
			nsize = normals.realSize(),
			usize = uv.realSize(),
			lsize = luma.realSize();
		
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vsize + nsize + usize + lsize, GL_STATIC_DRAW);
		
		nglBufferSubData(GL_ARRAY_BUFFER, 0, vsize, vertices.buffer());
		nglBufferSubData(GL_ARRAY_BUFFER, vsize, nsize, normals.buffer());
		nglBufferSubData(GL_ARRAY_BUFFER, nsize + vsize, usize, uv.buffer());
		nglBufferSubData(GL_ARRAY_BUFFER, usize + nsize + vsize, lsize, luma.buffer());
		
		clear();
		
		// MemoryUtil.memFree(buffer);
		
		vertexArraySize = vsize / 12;
		glBindVertexArray(vao);
		
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * 4, 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, false, 3 * 4, vsize);
		glVertexAttribPointer(2, 2, GL_FLOAT, false, 2 * 4, vsize + nsize);
		glVertexAttribPointer(3, 1, GL_FLOAT, false, 1 * 4, vsize + nsize + usize);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void copyVert(int pos) {
		vertices.add(vertices.getFloat(pos));
		vertices.add(vertices.getFloat(pos + 1));
		vertices.add(vertices.getFloat(pos + 2));
	}
	private void copyNormal(int pos) {
		normals.add(normals.getFloat(pos));
		normals.add(normals.getFloat(pos + 1));
		normals.add(normals.getFloat(pos + 2));
	}
	private void copyUV(int pos) {
		uv.add(uv.getFloat(pos));
		uv.add(uv.getFloat(pos + 1));
	}
	
	public boolean loaded() { return loaded; }
	public void unload() {
		loaded = false;
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBufferData(GL_ARRAY_BUFFER, 0, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	public void clear() {
		vertices.clear();
		normals.clear();
		uv.clear();
		luma.clear();
	}
	public Mesh quad() {
		int vsz = vertices.size();
		int nsz = normals.size();
		int usz = uv.size();
		int lsz = luma.size();
		
		copyVert(vsz - 6);
		copyVert(vsz - 9);
		if (nsz >= 9) {
			copyNormal(nsz - 6);
			copyNormal(nsz - 9);
		}
		if (usz >= 6) {
			copyUV(usz - 4);
			copyUV(usz - 6);
		}
		if (lsz >= 2) {
			luma.add(luma.getFloat(lsz - 1));
			luma.add(luma.getFloat(lsz - 2));
		}
		
		return this;
	}
	public Mesh addVertex(float x, float y, float z) {
		vertices.add(x);
		vertices.add(y);
		vertices.add(z);
		
		return this;
	}
	public Mesh addNormal(float x, float y, float z) {
		normals.add(x);
		normals.add(y);
		normals.add(z);
		
		return this;
	}
	public Mesh addUV(float x, float y) {
		uv.add(x);
		uv.add(y);
		
		return this;
	}
	public Mesh addLuma(float i) {
		luma.add(i);
		return this;
	}
}
