package de.matthil.glgraphics;

import java.io.IOException;

import org.joml.Matrix4f;

import static org.lwjgl.opengl.GL33.*;

public class Shader {
	private int vertexShader = -1, fragmentShader = -1, geometryShader = -1;
	private int shaderProgram = -1;
	private float[] mat4f = new float[16];
	
	public Shader() {}
	public Shader(String vertex, String fragment) {
		try {
			setVertexShader(vertex);
			setFragmentShader(fragment);
		} catch (IOException e) {
			throw new RuntimeException("Unable to load shaders (IO)");
		}
	}
	private int newShader(int shaderType, String content) {
		int shader = glCreateShader(shaderType);
		glShaderSource(shader, content);
		glCompileShader(shader);
		
		int status = glGetShaderi(shader, GL_COMPILE_STATUS);
		if (status != GL_TRUE) {
			System.err.println(glGetShaderInfoLog(shader));
			throw new RuntimeException("Shader compilation error");
		}
		
		return shader;
	}
	
	private int getUniform(String name) {
		return glGetUniformLocation(shaderProgram, name);
	}
	
	public void set(String name, Matrix4f mat) {
		use();
		glUniformMatrix4fv(getUniform(name), false, mat.get(mat4f));
	}
	
	public void set(String name, float x, float y, float z, float w) {
		use();
		glUniform4f(getUniform(name), x, y, z, w);
	}
	public void set(String name, float x, float y, float z) {
		use();
		glUniform3f(getUniform(name), x, y, z);
	}
	public void set(String name, float x, float y) {
		use();
		glUniform2f(getUniform(name), x, y);
	}
	public void set(String name, float value) {
		use();
		glUniform1f(getUniform(name), value);
	}
	
	public void set(String name, int i) {
		use();
		glUniform1i(getUniform(name), i);
	}
	public void set(String name, int x, int y) {
		use();
		glUniform2i(getUniform(name), x, y);
	}
	public void set(String name, int x, int y, int z) {
		use();
		glUniform3i(getUniform(name), x, y, z);
	}
	
	public void set(String name, float[] floats) {
		use();
		switch (floats.length) {
		case 1: glUniform1fv(getUniform(name), floats); break;
		case 2: glUniform2fv(getUniform(name), floats); break;
		case 3: glUniform3fv(getUniform(name), floats); break;
		case 4: glUniform4fv(getUniform(name), floats); break;
		default:
			throw new IllegalStateException("Invalid float array size");
		}
	}
	
	private int newShaderFromFile(int shaderType, String file) throws IOException {
		String fileContent = Util.loadResourceAsString(file);
		return newShader(shaderType, fileContent);
	}
	
	public void setVertexShader(String filename) throws IOException {
		vertexShader = newShaderFromFile(GL_VERTEX_SHADER, filename);
	}
	public void setVertexShaderFromString(String content) {
		vertexShader = newShader(GL_VERTEX_SHADER, content);
	}
	
	public void setFragmentShader(String filename) throws IOException {
		fragmentShader = newShaderFromFile(GL_FRAGMENT_SHADER, filename);
	}
	public void setFragmentShaderFromString(String content) {
		fragmentShader = newShader(GL_FRAGMENT_SHADER, content);
	}
	
	public void setGeometryShader(String filename) throws IOException {
		geometryShader = newShaderFromFile(GL_GEOMETRY_SHADER, filename);
	}
	public void setGeometryShaderFromString(String content) {
		geometryShader = newShader(GL_GEOMETRY_SHADER, content);
	}
	
	public void compile() {
		shaderProgram = glCreateProgram();
		if (vertexShader != -1) glAttachShader(shaderProgram, vertexShader);
		if (fragmentShader != -1) glAttachShader(shaderProgram, fragmentShader);
		if (geometryShader != -1) glAttachShader(shaderProgram, geometryShader);
		
		glLinkProgram(shaderProgram);
	}
	
	public void delete() {
		if (vertexShader != -1) glDeleteShader(vertexShader);
		if (fragmentShader != -1) glDeleteShader(fragmentShader);
		if (geometryShader != -1) glDeleteShader(geometryShader);
		glDeleteProgram(shaderProgram);
	}
	
	public void use() {
		glUseProgram(shaderProgram);
	}
}
