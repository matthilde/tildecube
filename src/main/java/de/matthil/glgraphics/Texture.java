package de.matthil.glgraphics;

import java.io.InputStream;
import java.nio.*;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.stb.STBImage.*;

public class Texture {
	private int tex = -1, width, height;
	
	public static int newTexture(int width, int height, int type, ByteBuffer buf) {
		int tex = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, tex);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	    glTexImage2D(GL_TEXTURE_2D, 0, type, width, height, 0, type, GL_UNSIGNED_BYTE, buf);
	    glGenerateMipmap(GL_TEXTURE_2D);
	    glBindTexture(GL_TEXTURE_2D, 0);
	 
	    return tex;
	}
	
	public Texture(int id, int width, int height) {
		this.width = width;
		this.height = height;
		this.tex = id;
	}
	
	public void bind() {
		glBindTexture(GL_TEXTURE_2D, tex);
	}
	public void unbind() {
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public void delete() {
		glDeleteTextures(tex);
	}
	
	public void loadTexture(ByteBuffer imbuf) {
		if (tex != -1) delete();
	    ByteBuffer buf;
	    if (imbuf == null)
	    	throw new IllegalStateException("Unable to load texture.");
	    // stbi_set_flip_vertically_on_load(true);
	    try (MemoryStack stack = MemoryStack.stackPush()) {
	    	IntBuffer width  = stack.mallocInt(1);
	    	IntBuffer height = stack.mallocInt(1);
	    	IntBuffer chans  = stack.mallocInt(1);
	    	buf = stbi_load_from_memory(imbuf, width, height, chans, 4);
	    	if (buf == null)
	    		throw new IllegalStateException("Unable to load texture.");
	    	this.width = width.get();
	    	this.height = height.get();
	    }
	    
	    tex = newTexture(width, height, GL_RGBA, buf);
	    
	    stbi_image_free(buf);
	}
	
	public void loadTextureFromStream(InputStream stream) {
		ByteBuffer imbuf = Util.loadFromStream(stream);
		loadTexture(imbuf);
		MemoryUtil.memFree(imbuf);
	}
	
	public void loadTextureFromResource(String filename) {
	    ByteBuffer imbuf = Util.loadResource(filename);
		loadTexture(imbuf);
	    MemoryUtil.memFree(imbuf);
	}
	
	public void loadTextureFromFile(String filename) {
		ByteBuffer imbuf = Util.loadFile(filename);
		loadTexture(imbuf);
		MemoryUtil.memFree(imbuf);
	}
	
	public Texture(String filename) {
		loadTextureFromResource(filename);
	}
	
	public int id() { return tex; }
	public int width() { return width; }
	public int height() { return height; }
}
