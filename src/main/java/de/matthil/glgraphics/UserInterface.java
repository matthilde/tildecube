package de.matthil.glgraphics;

public interface UserInterface {
	// Make an UI class thing
	
	// When the GUI is invoked
	public void onStart();
	
	// Can the user leave the menu by pressing escape?
	public boolean canEscape();
	// Does it pause the game when opened?
	public boolean pausesTheGame();
	// Game keys locked?
	public boolean blockedKeys();
	
	//// If game's locked
	// Key press event
	public void onKeyPress(Window window, int key);
	// Mouse press event
	public void onMousePress(Window window, float x, float y, int key);
	// Mouse move event
	public void onMouseMove(Window window, float x, float y);
	// Draw function
	public void draw(Window win);
	
	// Char press (basically gives a char instead of a keycode)
	public void onChar(Window window, int c);
}
