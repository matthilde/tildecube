package de.matthil.glgraphics;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.lwjgl.system.MemoryUtil;

public class Util {
	public static ByteBuffer loadResource(String filename) {
		InputStream stream = Util.class.getResourceAsStream(filename);
		if (stream == null) return null;
		
		return loadFromStream(stream);
	}
	
	public static ByteBuffer loadFile(String filename) {
		byte[] content;
		try {
			content = Files.readAllBytes(Paths.get(filename));
		} catch (IOException e) {
			return null;
		}
		
		ByteBuffer buf = MemoryUtil.memAlloc(content.length);
		buf.put(content).flip();
		return buf;
	}
	
	public static ByteBuffer loadFromStream(InputStream stream) {
		byte[] content;
		try {
			content = stream.readAllBytes();
		} catch (IOException e) {
			return null;
		}
		
		ByteBuffer buf = MemoryUtil.memAlloc(content.length);
		buf.put(content).flip();
		return buf;
	}
	
	public static String loadResourceAsString(String filename) {
		InputStream stream = Util.class.getResourceAsStream(filename);
		if (stream == null) return null;
		
		byte[] content;
		try {
			content = stream.readAllBytes();
		} catch (IOException e) {
			return null;
		}
	
		return new String(content);
	}
}
