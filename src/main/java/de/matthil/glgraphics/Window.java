package de.matthil.glgraphics;

import org.lwjgl.Version;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.io.IOException;
import java.util.logging.Logger;

import org.joml.Matrix4f;
import org.lwjgl.glfw.GLFWCharCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWFramebufferSizeCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWVidMode;

// Handles pretty much all the OpenGL boilerplate code
public abstract class Window {
	protected Logger log;
	
	protected long window;
	private int width, height;
	private String title;
	
	private boolean firstFrameMouse = true;
	private float prevMouseX, prevMouseY;
	
	private Matrix4f ortho;
	private Mesh HUDquad;
	private Shader HUDshader;
	
	private float previousTime;
	private float deltaTime;
	private float accumulatedTime = 0;
	private float fixedTime = 1f / 60f;
	
	public UserInterface currentUI = null;
	
	private Atlas font;
	private int fontWidth, fontHeight;
	
	private float[] uvTmp = new float[4];
	
	public Window(int width, int height, String title) {
		log = Logger.getLogger("de.matthil.glgraphics");
		log.info("Hello! Using LWJGL " + Version.getVersion());
		
		this.width = width;
		this.height = height;
		this.title = title;
		
		this.ortho = new Matrix4f().ortho(0, width, height, 0, -1, 1);
		init();
	}
	
	public int fontWidth() { return fontWidth; }
	public int fontHeight() { return fontHeight; }
	
	public float width() { return width; }
	public float height() { return height; }
	public float fixedTime() { return fixedTime; }
	
	public void draw(float dt) {}
	public void loop(float dt) {}
	public void fixedLoop(float dt) {}
	public void HUDloop(float dt) {}
	public void delete() {}
	public void onKeyPress(int key) {}
	public void onKeyRelease(int key) {}
	public void onMouseMove(float x, float y, float dx, float dy) {}
	public void onMousePress(int button) {}
	public void onMouseRelease(int button) {}
	public void onScroll(float x, float y) {}
	public void onResize(int width, int height) {}
	
	private void keyCallbackFunction(long window, int key, int scancode, int action, int mods) {
		if (currentUI != null && currentUI.canEscape() && action == GLFW_PRESS && key == GLFW_KEY_ESCAPE)
			openGUI(null);
		else if (currentUI == null || !currentUI.blockedKeys())
			switch (action) {
			case GLFW_PRESS:
				onKeyPress(key);
				break;
			case GLFW_RELEASE: onKeyRelease(key); break;
			}
		else if (action == GLFW_PRESS)
			currentUI.onKeyPress(this, key);
	}
	private void mouseClickCallbackFunction(long window, int button, int action, int mods) {
		if (currentUI == null || !currentUI.blockedKeys())
			switch (action) {
			case GLFW_PRESS: onMousePress(button); break;
			case GLFW_RELEASE: onMouseRelease(button); break;
			}
		else if (action == GLFW_PRESS)
			currentUI.onMousePress(this, prevMouseX, prevMouseY, button);
	}
	private void mouseCallbackFunction(long window, double mouseX, double mouseY) {
		float x = (float)mouseX, y = (float)mouseY;
		float dx, dy;
		
		if (firstFrameMouse) {
			firstFrameMouse = false;
			prevMouseX = x;
			prevMouseY = y;
		}
		dx = x - prevMouseX;
		dy = y - prevMouseY;
		prevMouseX = x;
		prevMouseY = y;
		
		if (currentUI == null || !currentUI.blockedKeys())
			onMouseMove(x, y, dx, dy);
		else
			currentUI.onMouseMove(this, x, y);
	}
	
	public void useHUDShader() {
		HUDshader.use();
	}
	
	public void clearColor(float r, float g, float b) {
		glClearColor(r, g, b, 1);
	}
	
	public void drawTexture(Texture tex, int x, int y) {
		useHUDShader();
		HUDshader.set("posSize", x, y, tex.width(), tex.height());
		HUDshader.set("uvOffset", 0, 0, 1, 1);
		tex.bind();
		HUDquad.draw();
		tex.unbind();
	}
	public void drawTextureEx(Texture tex, int x, int y, float width, float height, float scale, float[] uv) {
		useHUDShader();
		HUDshader.set("posSize", x, y, width * scale, height * scale);
		HUDshader.set("uvOffset", uv);
		tex.bind();
		HUDquad.draw();
		tex.unbind();
	}
	public void drawTextureFromAtlas(Atlas atlas, int tilex, int tiley, int x, int y, float scale) {
		atlas.getUV(uvTmp, x, y);
		drawTextureEx(atlas.texture(), x, y, atlas.tileWidth(), atlas.tileHeight(), scale, uvTmp);
	}
	public void drawTextureFromAtlas(Atlas atlas, int idx, int x, int y, float scale) {
		atlas.getUV(uvTmp, idx);
		drawTextureEx(atlas.texture(), x, y, atlas.tileWidth(), atlas.tileHeight(), scale, uvTmp);
	}
	
	public void drawRectangle(int x, int y, int w, int h, float r, float g, float b, float a) {
		useHUDShader();
		HUDshader.set("posSize", x, y, w, h);
		HUDshader.set("color", r, g, b, a);
		HUDquad.draw();
		HUDshader.set("color", 0, 0, 0, 0);
	}
	
	public void drawText(CharSequence s, int x, int y, float scale) {
		int ix = x;
		for (int i = 0; i < s.length(); ++i) {
			int c = s.charAt(i);
			if (c == '\n') {
				x = ix;
				y += font.tileHeight() * scale;
			} else {
				c -= ' ';
				drawTextureFromAtlas(font, c, x, y, scale);
				x += font.tileWidth() * scale;
			}
		}
	}
	
	public void openGUI(UserInterface ui) {
		setCursorEnabled(ui != null && ui.blockedKeys());
		currentUI = ui;
		
		firstFrameMouse = true;
		if (ui != null) ui.onStart();
	}

	@SuppressWarnings("unused")
	private void centerWindow() {
		GLFWVidMode mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(window, (mode.width() - width) / 2, (mode.height() - height) / 2);
	}
	
	public void init() {
		log.info("Initializing Window object...");
		if (!glfwInit())
			throw new IllegalStateException("Unable to init GLFW");
		
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		
		
		window = glfwCreateWindow(width, height, title, NULL, NULL);
		if (window == NULL)
			throw new IllegalStateException("Unable to init window.");
		
		
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1); // vsync
		
		glfwSetFramebufferSizeCallback(window, GLFWFramebufferSizeCallback.create((window, width, height) -> {
			glViewport(0, 0, width, height);
			this.width = width;
			this.height = height;
			this.ortho = new Matrix4f().ortho(0, width, height, 0, -1, 1);
			HUDshader.set("projection", ortho);
			
			onResize(width, height);
		}));
		
		glfwSetKeyCallback(window, GLFWKeyCallback.create(this::keyCallbackFunction));
		glfwSetCursorPosCallback(window, GLFWCursorPosCallback.create(this::mouseCallbackFunction));
		glfwSetMouseButtonCallback(window, GLFWMouseButtonCallback.create(this::mouseClickCallbackFunction));
		glfwSetScrollCallback(window, GLFWScrollCallback.create((window, x, y) -> {
			onScroll((float)x, (float)y);
		}));
		glfwSetCharCallback(window, GLFWCharCallback.create((window, c) -> {
			if (currentUI != null) currentUI.onChar(this, c);
		}));
		
		glfwShowWindow(window);
		
		// centerWindow();
		
		GL.createCapabilities();
		
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glViewport(0, 0, width, height);
		
		//// HUD necessary stuff ////
		
		HUDshader = new Shader();
		try {
			HUDshader.setVertexShader("/assets/hudvert.glsl");
			HUDshader.setFragmentShader("/assets/hudfrag.glsl");
		} catch (IOException e) {
			throw new IllegalStateException("Unable to load HUD shaders");
		}
		HUDshader.compile();
		HUDshader.set("projection", ortho);
		
		HUDquad = new Mesh();
		HUDquad.addVertex(0, 0, 0).addUV(0, 0)
		       .addVertex(1, 0, 0).addUV(1, 0)
		       .addVertex(0, 1, 0).addUV(0, 1)
		       .addVertex(1, 1, 0).addUV(1, 1);
		HUDquad.quad();
		HUDquad.commit();
		
		// Default font
		font = new Atlas("/assets/font.png").setTileSize(5, 12);
		fontWidth = 5;
		fontHeight = 12;
		
		previousTime = (float)glfwGetTime();
		
		log.info("Successfully initialized HUD shaders, HUD quad and GLFW window!");
	}
	
	public void quit() {
		glfwSetWindowShouldClose(window, true);
	}
	
	private void internalLoop() {
		while (!glfwWindowShouldClose(window)) {
			
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			if (currentUI == null || !currentUI.pausesTheGame()) {
				float currentTime = (float)glfwGetTime();
				deltaTime = currentTime - previousTime;
				previousTime = currentTime; 
				accumulatedTime += deltaTime;
			
				while (accumulatedTime >= fixedTime) {
					accumulatedTime -= fixedTime;
					fixedLoop(fixedTime);
				}
				loop(deltaTime);
			} else previousTime = (float)glfwGetTime();
			
			draw(deltaTime);
			
			glDisable(GL_DEPTH_TEST);
			
			HUDloop(deltaTime);
			if (currentUI != null)
				currentUI.draw(this);
			glEnable(GL_DEPTH_TEST);
			
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
	}
	
	public void setCursorEnabled(boolean enable) {
		glfwSetInputMode(window, GLFW_CURSOR, enable ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
	}
	
	public void run() {
		internalLoop();
		
		log.info("Bye!");
		
		delete();
		
		HUDshader.delete();
		HUDquad.delete();
		
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);
		
		glfwTerminate();
	}
}
