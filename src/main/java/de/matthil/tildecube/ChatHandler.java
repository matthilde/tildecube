package de.matthil.tildecube;

import de.matthil.tildecube.scripting.*;
import de.matthil.tildecube.ui.Chat;
import de.matthil.tildecube.ui.chat.ChatMessageEvent;

import java.io.IOException;

public class ChatHandler implements ChatMessageEvent {
	private static final String HELP_MESSAGE =
		"/help              - Show this help message\n" +
		"/tp x y z          - Teleport somewhere\n" +
		"/amogus            - spawn the mogus\n" +
		"/e <cmd>           - run Groovy code\n" +
		"/script <filename> - run a script from the scripts folder";
	
	private Tildecube game;
	private ScriptEngine script;
	
	public ChatHandler(Tildecube game) {
		script = game.scriptEngine;
		this.game = game;
	}
	
	public void printEvalResult(Chat chat, Object obj) {
		if (script.hasError())
			chat.message(script.getError());
		else if (obj != null)
			chat.message(script.toString(obj));
	}
	
	@Override
	public void run(Chat chat, String msg) {
		if (game.multiplayer != null) {
			if (msg.startsWith("//")) {
				String[] command = msg.substring(2).split(" ");
				if (command.length == 0) return;
				
				switch (command[0]) {
				case "e":
					if (msg.length() < 4) return;
					chat.message("groovy> " + msg.substring(4));
					printEvalResult(chat, script.eval(msg.substring(4)));
					break;
				case "script":
					if (msg.length() < 9) return;
					try {
						chat.message("Loaded " + msg.substring(9));
						printEvalResult(chat, script.evalFromFile("scripts/" + msg.substring(9)));
					} catch (IOException e) {
						chat.message("Unable to load " + msg.substring(9));
					}
					break;
				}
			} else {
				try {
					game.multiplayer.message(msg);
				} catch (IOException e) {
					game.crash(e);
				}
			}
			return;
		}
		if (msg.startsWith("/")) {
			String[] command = msg.substring(1).split(" ");
			if (command.length == 0) return;
			
			switch (command[0]) {
			case "help":
				chat.message(HELP_MESSAGE);
				break;
			case "tp":
				if (command.length != 4) break;
				float x = Float.parseFloat(command[1]);
				float y = Float.parseFloat(command[2]);
				float z = Float.parseFloat(command[3]);
				game.player.teleport(x, y, z);
				break;
			case "amogus":
				game.spawnAmogus();
				break;
			case "e":
				if (msg.length() < 3) return;
				chat.message("groovy> " + msg.substring(3));
				printEvalResult(chat, script.eval(msg.substring(3)));
				break;
			case "script":
				if (msg.length() < 8) return;
				try {
					chat.message("Loaded " + msg.substring(8));
					printEvalResult(chat, script.evalFromFile("scripts/" + msg.substring(8)));
				} catch (IOException e) {
					chat.message("Unable to load " + msg.substring(8));
				}
				break;
			}
		} else
			chat.message(String.format("<%s> %s", game.player.name(), msg));
	}
}
