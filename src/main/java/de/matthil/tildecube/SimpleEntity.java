package de.matthil.tildecube;

import org.joml.Vector3f;
import org.joml.Math;

import de.matthil.glgraphics.Camera;
import de.matthil.glgraphics.Mesh;
import de.matthil.glgraphics.Shader;
import de.matthil.tildecube.world.BlockType;
import de.matthil.tildecube.world.World;

public class SimpleEntity {
	public static final float GRAVITY = 0.3f;
	
	private String name;
	private float height;
	
	private World world = null;
	private Camera camera = null;
	private Mesh mesh = null;
	
	private float eyesHeight;
	private float fixedFramerate = 1f /60f;
	private boolean onGround = false, inWater = false, underWater = false;
	private Vector3f velocity = new Vector3f();
	private Vector3f prevPosition = new Vector3f();
	private Vector3f position = new Vector3f();
	private float rotation = 0;
	private float thickness = .3f;
	
	// Initializing some temporary vectors for convenience
	private Vector3f nextPosition = new Vector3f();
	private Vector3f tmp = new Vector3f();
	
	private boolean noclip = false;
	
	public SimpleEntity(String name, float height) {
		this.name = name;
		this.height = height;
	}
	
	private int voxelCollide(Vector3f position, Vector3f offset) {
		int bid;
		bid = world.getBlock((int)(position.x() - thickness), (int)position.y(), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(-thickness, 0, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() - thickness), (int)position.y(), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(-thickness, 0, thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)position.y(), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(thickness, 0, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)position.y(), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(thickness, 0, thickness); return bid; }
		
		bid = world.getBlock((int)(position.x() - thickness), (int)(position.y() + height), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(-thickness, height, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() - thickness), (int)(position.y() + height), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(-thickness, height, thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)(position.y() + height), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(thickness, height, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)(position.y() + height), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(thickness, height, thickness); return bid; }
		
		return 0;
	}
	
	private float handleCollision(int attr, float velocity) {
		nextPosition.set(position).setComponent(attr, position.get(attr) + velocity);
		
		int bid = voxelCollide(nextPosition, tmp);
		if (bid > 0 && world.blocks().get(bid - 1).translucent == BlockType.FLUID) {
			inWater = onGround = true;
		} else if (bid > 0) {
			float offset = tmp.get(attr);
			float p = Math.floor(position.get(attr) + offset);
			float np = Math.floor(nextPosition.get(attr) + offset);
			float invSignum  = p - np;
			float difference = invSignum * velocity;
			if (attr == 1 && invSignum > 0)
				onGround = true;
			
			velocity -= invSignum * difference;
		}
		
		return velocity;
	}
	
	private void updatePosition() {
		prevPosition.set(position);
		// position.add(velocity, nextPosition);
		onGround = inWater = underWater = false;
		
		// velocity.mul(1, 0, 0, tmpVelocity);
		if (!noclip) {
			if (velocity.x != 0) velocity.x = handleCollision(0, velocity.x); 
			if (velocity.y != 0) velocity.y = handleCollision(1, velocity.y); 
			if (velocity.z != 0) velocity.z = handleCollision(2, velocity.z); 
		}
		
		position.add(velocity.mul(inWater ? 0.75f : 1, tmp));
		if (camera != null) {
			camera.move(position.x(), position.y() + eyesHeight, position.z());
			
			// Underwater effect for camera
			int bid = world.getBlock((int)camera.x(), (int)camera.y(), (int)camera.z());
			if (bid > 0 && world.blocks().get(bid - 1).translucent == BlockType.FLUID)
				underWater = true;
		}
		if (mesh != null)   mesh.setPosition(position.x(), position.y() + (height / 2), position.z());
	}
	
	public void draw(Shader shader) {
		shader.set("transform", mesh.getTransform());
		mesh.draw();
	}
	
	public boolean isOnGround() { return onGround; }
	public boolean isInWater() { return inWater; }
	public boolean isUnderWater() { return underWater; }
	public String name() { return name; }
	
	public void bindCamera(Camera camera, float eyes) {
		this.camera = camera;
		this.eyesHeight = eyes;
	}
	public void bindWorld(World world) {
		this.world = world;
	}
	public void bindMesh(Mesh mesh) {
		this.mesh = mesh;
	}
	public void setFixedFramerate(float framerate) {
		fixedFramerate = 1f / framerate;
	}
	
	public void teleport(float x, float y, float z) {
		position.set(x, y, z);
		prevPosition.set(position);
		velocity.zero();
	}
	public void teleport(Vector3f pos) {
		position.set(pos);
		prevPosition.set(pos);
		velocity.zero();
	}
	
	public void setNoclipMode(boolean f) {
		noclip = world == null || f;
	}
	public boolean noclip() { return noclip; }
	public Vector3f velocity() { return velocity; }
	public Vector3f position() { return position; }
	public float rotation() { return rotation; }
	public Camera camera() { return camera; }
	public Mesh mesh() { return mesh; }
	public World world() { return world; }
	
	public void setVelocityX(float x) { velocity.x = x * fixedFramerate; }
	public void setVelocityY(float y) {
		if (y > 0) onGround = false;
		velocity.y = y * fixedFramerate;
	}
	public void setVelocityZ(float z) { velocity.z = z * fixedFramerate; }
	public void setRotation(float ang) { rotation = ang; }
	
	public void rotate(float ang) { rotation += ang; }
	
	public void update() {
		if (!(onGround || noclip || inWater)) velocity.sub(0, GRAVITY * fixedFramerate, 0);
		updatePosition();
	}
}
