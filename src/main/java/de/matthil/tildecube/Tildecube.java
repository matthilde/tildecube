package de.matthil.tildecube;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.zip.*;

import de.matthil.glgraphics.*;
import de.matthil.tildecube.generation.*;
import de.matthil.tildecube.handling.*;
import de.matthil.tildecube.mc.ClassicWorld;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.scripting.ScriptEngine;
import de.matthil.tildecube.ui.*;
import de.matthil.tildecube.ui.widgets.*;
import de.matthil.tildecube.world.Block;
import de.matthil.tildecube.world.World;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Math;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.util.nfd.NativeFileDialog.*;

// Main program, handles pretty much all the stuff
public class Tildecube extends Window {
	Logger log;
	
	// Basic stuff such as camera, shader, etc.
	Shader shader;
	Vector3f tmp;
	Matrix4f perspective;
	
	// Player
	public Camera camera; float fov = Math.toRadians(70);
	public SimpleEntity player;
	public float playerMoveX = 0, playerMoveZ = 0, playerMoveY = 0;
	public float playerSpeed = 4;
	
	// Entities
	Texture entityTexture;
	ArrayList<SimpleEntity> entities = new ArrayList<SimpleEntity>();
	
	// Multiplayer entities
	SimpleEntity[] netPlayers;
	Mesh netPlayerMesh;
	
	// World
	// World world; Block[] blocks;
	// Atlas blockAtlas; int pickedBlock = 1;
	public Atlas blockAtlas;
	
	// Oh no! It's the new world order!
	public World world;
	public ArrayList<Block> blocks;
	Shader voxelShader;
	
	// Some user parameters
	boolean cursorEnabled = true;
	int renderDistance = 2;
	
	// Raycasting information buffers
	int[] raycastPos    = new int[3];
	int[] raycastNormal = new int[3];
	
	//// Groovy
	ScriptEngine scriptEngine;
	EventManager events;
	
	//// Minecraft Classic Protocol (cursed)
	String ip; int port;
	String name, mppass;
	public ClassicClient multiplayer = null;
	
	//// GUI stuff
	public Atlas guiAtlas;
	
	// User interfaces and menus
	BlockPicker blockPicker;
	Menu pauseMenu, generatorMenu;
	
	// Selection cube stuff
	Texture cross, selectTexture;
	boolean cubeSelected;
	Mesh selectCube;
	
	// Inventory
	public Inventory inventory;
	// Chat
	public Chat chat;
	
	// frame interval-related variables
	float timeAccumulator = 0;
	float tick = 0.25f;
	float fpsCount = 0;
	
	Tildecube(int width, int height, String title) {
		super(width, height, title);
		log = Logger.getLogger("de.matthil.tildecube");
		
		start(false);
	}
	Tildecube(int width, int height, String title, String ip, int port, String name, String mppass) {
		super(width, height, title);
		log = Logger.getLogger("de.matthil.tildecube");
		
		this.ip = ip;
		this.port = port;
		this.name = name;
		this.mppass = mppass;
		start(true);
	}
	
	public void processInput() {
		if (glfwGetKey(super.window, GLFW_KEY_SPACE) == GLFW_PRESS && player.isOnGround())
			player.setVelocityY(playerSpeed * 2);
	}
	
	@Override
	public void onKeyPress(int key) {
		switch (key) {
		//// Movement keys ////
		case GLFW_KEY_W:
			playerMoveX = -1;
			break;
		case GLFW_KEY_S:
			playerMoveX = 1;
			break;
		case GLFW_KEY_A:
			playerMoveZ = -1;
			break;
		case GLFW_KEY_D:
			playerMoveZ = 1;
			break;
		case GLFW_KEY_SPACE:
			playerMoveY = 1;
			break;
		case GLFW_KEY_Q:
			playerMoveY = -1;
			break;
		case GLFW_KEY_LEFT_SHIFT:
			playerSpeed = 12;
			break;
			
		//// Game menu keys
		// Open block picker
		case GLFW_KEY_ESCAPE:
			if (super.currentUI == null) super.openGUI(pauseMenu);
			// toggleCursor();
			break;
		case GLFW_KEY_B:
			super.openGUI(blockPicker);
			break;
			
		//// Modifier and config keys ////
		// T - chat
		case GLFW_KEY_T:
			super.openGUI(chat);
			break;
		// F - Toggle noclip
		case GLFW_KEY_F:
			player.setNoclipMode(!player.noclip());
			break;
		// F1 - toggle cursor
		case GLFW_KEY_F1:
			toggleCursor();
			break;
			/*
		case GLFW_KEY_F2:
			String cwFilename = Util.openFileDialog();
			blockAtlas.texture().loadTextureFromFile(cwFilename);
			break;
		case GLFW_KEY_F3:
			String filename = Util.openFileDialog();
			guiAtlas.texture().loadTextureFromFile(filename);
			break;
			*/
			// Debug menu
		default: break;
		}
	}
	
	@Override
	public void onKeyRelease(int key) {
		switch (key) {
		case GLFW_KEY_W: case GLFW_KEY_S:
			playerMoveX = 0;
			break;
		case GLFW_KEY_A: case GLFW_KEY_D:
			playerMoveZ = 0;
			break;
		case GLFW_KEY_SPACE: case GLFW_KEY_Q:
			playerMoveY = 0;
			break;
		case GLFW_KEY_LEFT_SHIFT:
			playerSpeed = 4;
			break;
		default: break;
		}
	}
	
	public int[] getRaycastPosition() {
		if (!cubeSelected) return null;
		return raycastPos.clone();
	}
	public int[] getRaycastNormal() {
		if (!cubeSelected) return null;
		return raycastNormal.clone();
	}
	
	@Override
	public void onMousePress(int button) {
		if (cubeSelected) {
			int bid = getBlock(raycastPos[0], raycastPos[1], raycastPos[2]) - 1;
			Block block = blocks.get(bid);
			switch (button) {
			// Destroy block
			case GLFW_MOUSE_BUTTON_1:
				setBlock(raycastPos[0], raycastPos[1], raycastPos[2], 0);
				block.triggerEvent(this, world, raycastPos[0], raycastPos[1], raycastPos[2], true);
				break;
			// Place block
			case GLFW_MOUSE_BUTTON_2:
				if (block.handler == null)
					setBlock(raycastPos[0] + raycastNormal[0], raycastPos[1] + raycastNormal[1], raycastPos[2] + raycastNormal[2], inventory.getCurrentBlock() + 1);
				else
					block.triggerEvent(this, world, raycastPos[0], raycastPos[1], raycastPos[2], false);
				break;
			// Pick block
			case GLFW_MOUSE_BUTTON_3:
				inventory.handleMiddleClick(bid);
				break;
			}
			// world.updateMesh();
			updateSelectCube();
		}
	}
	
	@Override
	public void onMouseMove(float x, float y, float dx, float dy) {
		if (!cursorEnabled) {
			camera.rotateRelative(-dy / 2, dx / 2);
			updateSelectCube();
		}
	}
	
	@Override
	public void onScroll(float x, float y) {
		if (y < 0) inventory.nextSlot();
		else if (y > 0) inventory.prevSlot();
	}
	
	@Override
	public void onResize(int width, int height) {
		float ratio = (float)width / (float)height;
		perspective = (new Matrix4f()).perspective(fov, ratio, 0.1f, 400);
		camera.setPerspectiveMatrix(perspective);
		shader.set("perspective", perspective);
		voxelShader.set("perspective", perspective);
	}
	
	private void updateSelectCube() {
		if (cubeSelected = Util.raycastVoxels(camera, world, raycastPos, raycastNormal, 5.0f))
			selectCube.setPosition(raycastPos[0] + .5f, raycastPos[1] + .5f, raycastPos[2] + .5f);
		else
			selectCube.setPosition(0, -100, 0);
	}

	public void setFog(float r, float g, float b, float distance) {
		shader.set("fogColor", r, g, b);
		voxelShader.set("fogColor", r, g, b);

		setFog(distance);
	}
	public void setFog(float distance) {
		// distance = (float)java.lang.Math.log(0.01) / (distance * 0.99f);
		shader.set("fogEnd", distance);
		voxelShader.set("fogEnd", distance);
	}

	public void setRenderDistance(int distance) {
		setFog(32 * distance);
		world.setChunkRenderDistance(distance);
		renderDistance = distance;
	}
	
	public void toggleCursor() {
		super.setCursorEnabled(cursorEnabled = !cursorEnabled);
	}
	
	public void crash(Exception e) {
		e.printStackTrace();
		quit();
	}
	
	private void loadTextureFromZip(Texture t, ZipFile f, String filename) throws IOException {
		ZipEntry entry = f.getEntry(filename);
		t.loadTextureFromStream(f.getInputStream(entry));
	}
	private void loadTexturePack(String filename) {
		try {
			ZipFile f = new ZipFile(filename);
			loadTextureFromZip(blockAtlas.texture(), f, "terrain.png");
			loadTextureFromZip(guiAtlas.texture(), f, "gui.png");
		} catch (IOException e) {
			crash(e);
		}
	}
	
	private Button generateLoadButton(int y, int wn, Path dir, boolean load) {
		Path file = dir.resolve("world" + wn + ".cw");
		if (!Files.exists(file))
			return new Button(y, "(empty)", (self, window) -> {
				if (!load) {
					try {
						ClassicWorld.saveWorld(world, player, file.toString());
					} catch (IOException e) {
						chat.message("Unable to load/save world.");
					}
					
					window.openGUI(null);
				}
			});
		
		return new Button(y, "World " + wn, (self, window) -> {
			try {
				if (load)
					ClassicWorld.loadWorld(world, player, file.toString());
				else
					ClassicWorld.saveWorld(world, player, file.toString());
			} catch (IOException e) {
				chat.message("Unable to load/save world " + wn);
			}
			window.openGUI(null);
		});
	}
	
	private Menu generateWorldSelectionMenu(boolean load) {
		Menu menu = new Menu(guiAtlas);
		
		Path p = Paths.get("./worlds");
		try {
			if (Files.exists(p) && !(Files.isDirectory(p)))
				Files.delete(p);
			if (!Files.exists(p))
				Files.createDirectory(p);
		} catch (IOException e) {
			crash(e);
		}
		
		if (load) {
			menu.addWidget(new SizedButton(-100, 150, 100, "Import...", (self, window) -> {
				String cwFilename = Util.openFileDialog();
				if (cwFilename != null) { 
					try {
						ClassicWorld.loadWorld(world, player, cwFilename);
					} catch (IOException e) {
						log.warning("Unable to load world.");
					}
				}
				window.openGUI(null);
			}));
		} else {
			menu.addWidget(new SizedButton(-100, 150, 100, "New...", (self, window) -> { 
				window.openGUI(generatorMenu);
			}));
		}
		menu.addWidget(new SizedButton(100, 150, 100, "Back", (self, window) -> { super.openGUI(pauseMenu); }));
		menu.addWidget(new Label(-150, "Pick a world"));
		menu.addWidget(generateLoadButton(-100, 0, p, load));
		menu.addWidget(generateLoadButton(-50,  1, p, load));
		menu.addWidget(generateLoadButton(0,    2, p, load));
		menu.addWidget(generateLoadButton(50,   3, p, load));
		menu.addWidget(generateLoadButton(100,  4, p, load));
		
		return menu;
	}
	
	public void generatePauseMenu(boolean multiplayer) {
		// Generator menu
		generatorMenu = new Menu(guiAtlas);
		generatorMenu.addWidget(new Label(-100, "Select a generator"));
		generatorMenu.addWidget(new Button(-50, "Overworld", (self, window) -> {
			world.resize(4, 2, 4);
			Worldgen w = new Worldgen((long)glfwGetTime(), 128, 64, 128);
			w.generate(world);
			
			player.teleport(64, 48, 64);
			window.openGUI(null);
		}));
		generatorMenu.addWidget(new Button(0, "Perlin Overworld", (self, window) -> {
			world.resize(16, 2, 16);
			FunGenerators.perlinTerrain(world);
			player.teleport(256, 33, 256);
			window.openGUI(null);
		}));
		generatorMenu.addWidget(new Button(50, "Caves", (self, window) -> {
			world.resize(8, 8, 8);
			FunGenerators.caves(world);
			player.teleport(128, 256, 128);
			window.openGUI(null);
		}));
		generatorMenu.addWidget(new Button(100, "Flatland", (self, window) -> {
			world.resize(16, 2, 16);
			FunGenerators.flatland(world);
			player.teleport(256, 5, 256);
			window.openGUI(null);
		}));
		
		// Pause menu
		pauseMenu = new Menu(guiAtlas);
		pauseMenu.addWidget(new Label(-100, "-- PAUSE MENU --"));
		if (!multiplayer) {
			pauseMenu.addWidget(new SizedButton(-100, -50, 100, "Load", (self, window) -> { window.openGUI(generateWorldSelectionMenu(true)); }));
			pauseMenu.addWidget(new SizedButton(100, -50, 100, "Save/New", (self, window) -> { window.openGUI(generateWorldSelectionMenu(false)); }));
		}
		pauseMenu.addWidget(new Button(50, "Load texture pack", (self, window) -> {
			String filename = Util.openFileDialog();
			loadTexturePack(filename);
		}));
		pauseMenu.addWidget(new Button(0, String.format("Render distance: %d", renderDistance),
			(self, window) -> {
				renderDistance += 1;
				if (renderDistance > 6) renderDistance = 1;
				setRenderDistance(renderDistance);

				self.setLabel(String.format("Render distance: %d", renderDistance));
				log.info(String.format("Set render distance to %d", renderDistance));
			}
		));
		
		pauseMenu.addWidget(new Button(100, "Quit game", (self, window) -> {
			glfwSetWindowShouldClose(super.window, true);
		}));
		pauseMenu.pausesTheGame(!multiplayer);
	}
	
	public void start(boolean multiplayer) {
		//// INITIALIZE EVERYTHING ////
		
		log.info("Starting game...");
		toggleCursor();
		
		tmp = new Vector3f();
		
		// Set perspective matrix
		float ratio = super.width() / super.height();
		perspective = (new Matrix4f()).perspective(fov, ratio, 0.1f, 400);
		
		// Set camera
		camera = new Camera();
		camera.setPerspectiveMatrix(perspective);
		
		// Load cross texture
		cross = new Texture("/assets/cross.png");
		
		// Load GUI texture
		guiAtlas = new Atlas("/assets/gui.png");
		
		// Set up and load the selector cube
		cubeSelected = false;
		selectCube = Primitives.newCube();
		selectCube.setScale(1.01f);
		selectTexture = new Texture("/assets/select.png");
		
		// Load game shader
		shader = new Shader();
		try {
			shader.setVertexShader("/assets/vert.glsl");
			shader.setFragmentShader("/assets/frag.glsl");
		} catch (IOException e) {
			throw new RuntimeException("Unable to load shaders (IO)");
		}
		shader.compile();
		shader.set("perspective", perspective);
		
		// New world stuff incoming
		voxelShader = new Shader("/assets/voxel/vertex.glsl", "/assets/frag.glsl");
		voxelShader.compile();
		voxelShader.set("perspective", perspective);

		setFog(135f/255f, 206f/255f, 250f/255f, 1.0f);
		
		blockAtlas = new Atlas("/assets/terrain.png").subdivide(16, 16);
		
		// Set up the world and the block set
		blocks = Block.defaultBlocks();
		int sz = 2;
		world = new World(voxelShader, blockAtlas.texture(), blocks, sz, 2, sz);
		setRenderDistance(2);

		// Set player
		player = new SimpleEntity("Player", 1.8f);
		player.bindCamera(camera, 1.725f);
		player.bindWorld(world);
		player.setNoclipMode(true);
		
		// Multiplayer
		if (multiplayer) {
			log.info("Connecting to server...");
			this.multiplayer = new ClassicClient();
			try {
				this.multiplayer.connect(ip, port, name, mppass);
			} catch (IOException e) {
				crash(e);
			}
			
			netPlayers = new SimpleEntity[256];
			for (int i = 0; i < 256; ++i)
				netPlayers[i] = null;
		} else {
			// Procedurally generate a new terrain if no map is there.
			log.info(String.format("Size is %d.", sz));
			
			world.resize(4, 2, 4);
			Worldgen w = new Worldgen((long)glfwGetTime(), 128, 64, 128);
			w.generate(world);
			
			player.teleport(64, 48, 64);
			player.update();
			
			this.multiplayer = null;
		}
		
		// Entity stuff
		entityTexture = new Texture("/assets/amog.png"); // placeholder texture thing
		
		// Javascript
		scriptEngine = new ScriptEngine(this);
		events = new EventManager();
		
		// Chat
		chat = new Chat();
		chat.setOnUserMessageHandler(new ChatHandler(this));
		// Inventory
		inventory = new Inventory(guiAtlas, blockAtlas, blocks);
		// Load menus
		blockPicker = new BlockPicker(inventory, blocks, blockAtlas);
		
		// Menus
		generatePauseMenu(multiplayer);
	}
	
	public void spawnAmogus() {
		log.info("Spawned a new entity!");
		SimpleEntity ent = new SimpleEntity(null, 1f);
		ent.bindWorld(world);
		ent.bindMesh(Primitives.newCube());
		ent.teleport(player.position());
		entities.add(ent);
	}
	
	public SimpleEntity[] netPlayers() { return netPlayers; }
	public SimpleEntity spawnNetPlayer(int id) {
		SimpleEntity ent = new SimpleEntity(null, 1f);
		ent.bindMesh(Primitives.newCube());
		ent.teleport(0, 0, 0);
		ent.setNoclipMode(true);
		
		netPlayers[id] = ent;
		return ent;
	}
	public void deleteNetPlayer(int id) {
		netPlayers[id] = null;
	}
	
	public EventManager eventManager() { return events; }
	public void addEventHandler(String name, EventHandler event) {
		events.pushEvent(name, event);
		event.start(this);
	}
	public void removeEventHandler(String name) {
		events.deleteEvent(name);
	}
	
	public void setBlock(int x, int y, int z, int bid) {
		setBlock(x, y, z, bid, false);
	}
	public void setBlock(int x, int y, int z, int bid, boolean silent) {
		// If multiplayer is on, then it's ClassicClient that will place blocks.
		if (multiplayer == null) {
			int[] info = { x, y, z, world.getBlock(x, y, z), bid };
			world.setBlock(x, y, z, bid);
			if (!silent)
				events.block(this, info);
		} else {
			try {
				multiplayer.setBlock(x, y, z, bid);
			} catch (IOException e) {
				crash(e);
			}
		}
	}
	public int getBlock(int x, int y, int z) {
		return world.getBlock(x, y, z);
	}
	
	@Override
	public void HUDloop(float dt) {
		if (player.isUnderWater())
			drawRectangle(0, 0, (int)width(), (int)height(), 135f/255f, 206f/255f, 250f/255f, 0.4f);
		
		int x = (int)super.width() / 2 - cross.width() / 2;
		int y = (int)super.height() / 2 - cross.height() / 2;
		drawTexture(cross, x, y);
		
		// drawTextureFromAtlas(blockAtlas, blocks[pickedBlock].forward, (int)super.width() - 36, 4, 2);
		inventory.draw(this);
		
		String debugInfo = String.format(
			"Tildecube devbuild - %d FPS\n" +
			"Position: %.2f %.2f %.2f\n" +
			"Chunk: %d %d %d",
			(int)fpsCount,
			player.position().x(), player.position().y(), player.position().z(),
			(int)player.position().x() >> 5, (int)player.position().y() >> 5, (int)player.position().z() >> 5
		);
		drawText(debugInfo, 2, 2, 2);
		
		if (currentUI != chat)
			chat.drawChatWindow(this);
	}
	
	public void tick(float dt) {
		fpsCount = 1f / dt;
		
		events.tick(this, dt);
		
		if (multiplayer != null) {
			try {
				multiplayer.move(player);
			} catch (IOException e) {
				crash(e);
			}
		}
		
		for (SimpleEntity entity : entities) {
			if (!entity.isOnGround())
				continue;
			else {
				entity.setVelocityX(0);
				entity.setVelocityZ(0);
			}
			int r = (int)(Math.random() * 6);
			if (r == 4) {
				entity.setVelocityY(8);
				entity.setVelocityX((float)Math.random() * 8 - 4);
				entity.setVelocityZ((float)Math.random() * 8 - 4);
			}
		}
	}
	
	@Override
	public void fixedLoop(float dt) {
		if (playerMoveX != 0 || playerMoveZ != 0) {
			tmp.set(playerMoveX, 0, playerMoveZ).normalize(playerSpeed).rotateY(Math.toRadians(camera.yaw()));
			player.setVelocityX(-tmp.x);
			player.setVelocityZ(tmp.z);
		} else {
			player.setVelocityX(0);
			player.setVelocityZ(0);
		}
		
		if (player.noclip()) {
			player.setVelocityY(playerMoveY * playerSpeed);
		}
		
		if (multiplayer != null) {
			for (SimpleEntity entity : netPlayers)
				if (entity != null) entity.update();
		}
		player.update();
		for (SimpleEntity entity : entities)
			entity.update();
	}
	
	@Override
	public void draw(float dt) {
		super.clearColor(135f/255f, 206f/255f, 250f/255f);
		
		shader.use();
		shader.set("camera", camera.getCameraMatrix());
		shader.set("transform", selectCube.getTransform());
		
		// Block selection cube
		selectTexture.bind();
		selectCube.draw();
		selectTexture.unbind();
		
		// Entities
		entityTexture.bind();
		for (SimpleEntity entity : entities)
			entity.draw(shader);
		if (multiplayer != null) {
			for (SimpleEntity entity : netPlayers) {
				if (entity != null)
					entity.draw(shader);
			}
		}
		entityTexture.unbind();
	
		// World
		world.draw(camera);
	}
	
	@Override
	public void loop(float dt) {
		timeAccumulator += dt;
		if (timeAccumulator > tick) {
			timeAccumulator = 0;
			
			tick(dt);
		}
		
		world.loadChunkFromQueue();
		world.loadChunkFromQueue();
		world.loadChunkFromQueue();
		world.loadChunkFromQueue();
		
		if (multiplayer != null) {
			try {
				while (multiplayer.handlePacket(this));
			} catch (IOException e) {
				crash(e);
			}
		}
		
		events.update(this, dt);
		
		if (super.currentUI == null)
			processInput();
		updateSelectCube();
	}
	
	@Override
	public void delete() {
		log.info("Bye!");
		if (multiplayer == null) {
			try {
				// world.save(new File("save.map"), player.position());
				ClassicWorld.saveWorld(world, player, "save.cw");
			} catch (IOException e) {}
		}
		if (multiplayer != null) {
			multiplayer.disconnect();
			for (SimpleEntity entity : netPlayers)
				if (entity != null) entity.mesh().delete();
		}
		
		for (SimpleEntity entity : entities)
			entity.mesh().delete();
		
		blockAtlas.delete();
		voxelShader.delete();
		
		selectTexture.delete();
		selectCube.delete();
		
		shader.delete();
		world.delete();
	}
	
	public static void main(String[] args) {
		// Totally not doing like Terraria
		String[] funnyQuotes = {
			"jaaj cooc",
			"Now on estrogen!",
			"bnuuy! bnuuy! bnuuy!",
			"made by matthilde",
			"If you read this, yes you have spotted I imitated Terraria.",
			"Yet another Miencraft clone!",
			"Written in the worst programming language ever",
			"Written once, hasn't debugged everywhere",
			"Maxxximum Minecraft Clones!",
			"tech demo",
			"Guys im 10 months sober",
			"trans rights",
			"will add",
			"runs on 3 billion devices yet can't run on the fucking wii",
			"classicube 3ds when",
			"Does not explicity call System.gc()!",
			"woof",
			"bnuuy"
		};
		String quote = funnyQuotes[(int)(Math.random() * funnyQuotes.length)];
		
		// Initalize NativeFileDialog
		if (NFD_Init() != NFD_OKAY)
			throw new RuntimeException("Unable to init Native File Dialog.");
		
		// Totally not minecraft default resolution
		// Tildecube game = new Tildecube(854, 480, "Tildecube - " + quote);
		Tildecube game;
		if (args.length == 4) {
			String ip = args[0];
			int port = Integer.valueOf(args[1]);
			String name = args[2];
			String mppass = args[3];
			
			game = new Tildecube(854, 480, "Tildecube - " + quote, ip, port, name, mppass);
		} else
			game = new Tildecube(854, 480, "Tildecube - " + quote);
		game.run();
		
		NFD_Quit();
	}
}
