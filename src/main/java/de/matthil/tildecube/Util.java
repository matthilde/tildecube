package de.matthil.tildecube;

import de.matthil.glgraphics.Camera;
import de.matthil.tildecube.world.BlockType;
import de.matthil.tildecube.world.World;

import org.joml.Vector3f;
import org.joml.Math;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.util.nfd.NFDFilterItem;
import org.lwjgl.PointerBuffer;

import static org.lwjgl.util.nfd.NativeFileDialog.*;

public class Util {
	static private float infinity = Float.POSITIVE_INFINITY;
	
	public static String openFileDialog() {
		String result = null;
		
		try (MemoryStack stack = MemoryStack.stackPush()) {
			PointerBuffer path = stack.mallocPointer(1);
			NFDFilterItem.Buffer filter = new NFDFilterItem.Buffer(stack.malloc(0));
			int r = NFD_OpenDialog(path, filter, (String)null);
			
			if (r == NFD_OKAY) {
				result = path.getStringASCII();
				NFD_FreePath(path.get(0));
			}
		}
		
		return result;
	}
	
	// DDA Algorithm taken from
	// https://github.com/fenomas/fast-voxel-raycast/blob/master/index.js 
	public static boolean raycastVoxels(Camera cam, World world, int[] result, int[] normal, float maxDistance) {
		float t = 0;
		Vector3f pos = cam.position(), dir = cam.direction();
		int ix = (int)pos.x, iy = (int)pos.y, iz = (int)pos.z;
		int stepX = dir.x() > 0 ? 1 : -1,
			stepY = dir.y() > 0 ? 1 : -1,
			stepZ = dir.z() > 0 ? 1 : -1,
			steppedIndex = -1;
		
		float txDelta = Math.abs(1f / dir.x),
			  tyDelta = Math.abs(1f / dir.y),
			  tzDelta = Math.abs(1f / dir.z),
			  xDist = (stepX > 0) ? (float)ix + 1 - pos.x : pos.x - (float)ix,
			  yDist = (stepY > 0) ? (float)iy + 1 - pos.y : pos.y - (float)iy,
			  zDist = (stepZ > 0) ? (float)iz + 1 - pos.z : pos.z - (float)iz,
			  txMax = (txDelta < infinity) ? txDelta * xDist : infinity,
			  tyMax = (tyDelta < infinity) ? tyDelta * yDist : infinity,
			  tzMax = (tzDelta < infinity) ? tzDelta * zDist : infinity;
		
		while (t <= maxDistance) {
			int bid = world.getBlock(ix, iy, iz);
			if (bid > 0 && world.blocks().get(bid - 1).translucent != BlockType.FLUID) {
				result[0] = ix;
				result[1] = iy;
				result[2] = iz;
				normal[0] = normal[1] = normal[2] = 0;
			
				switch (steppedIndex) {
				case 0: normal[0] = -stepX; break;
				case 1: normal[1] = -stepY; break;
				case 2: normal[2] = -stepZ; break;
				}
				
				return true;
			}
			
			if (txMax < tyMax) {
				if (txMax < tzMax) {
					ix += stepX;
					t = txMax;
					txMax += txDelta;
					steppedIndex = 0;
				} else {
					iz += stepZ;
					t = tzMax;
					tzMax += tzDelta;
					steppedIndex = 2;
				}
			} else {
				if (tyMax < tzMax) {
					iy += stepY;
					t = tyMax;
					tyMax += tyDelta;
					steppedIndex = 1;
				} else {
					iz += stepZ;
					t = tzMax;
					tzMax += tzDelta;
					steppedIndex = 2;
				}
			}
		}
		
		return false;
	}
}
