package de.matthil.tildecube.entity;

import org.joml.Vector3f;
import de.matthil.glgraphics.Mesh;

public class ModelMember {
	public final Vector3f position, rotation;
	public final Mesh mesh;
	
	public ModelMember(Mesh mesh, Vector3f position, Vector3f rotation) {
		this.mesh = mesh;
		this.position = position;
		this.rotation = rotation;
	}
	public ModelMember(Mesh mesh, Vector3f position) {
		this.mesh = mesh;
		this.position = position;
		this.rotation = new Vector3f(0);
	}
}
