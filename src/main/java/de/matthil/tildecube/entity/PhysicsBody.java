package de.matthil.tildecube.entity;

import de.matthil.tildecube.world.BlockType;
import de.matthil.tildecube.world.World;

import org.joml.Vector3f;
import org.joml.Math;

public class PhysicsBody {
	private float fixedFramerate = 1f / 60f;
	
	private World world;
	
	private float gravity;
	private final float height;
	private final float thickness;
	
	private boolean onGround = false, inWater = false;
	private Vector3f velocity = new Vector3f();
	private Vector3f prevPosition = new Vector3f();
	private Vector3f position = new Vector3f();
	private boolean noclip = false;
	
	// Initializing some temporary vectors for convenience
	private Vector3f nextPosition = new Vector3f();
	private Vector3f tmp = new Vector3f();
	
	public PhysicsBody(float gravity, float height, float thickness) {
		this.gravity = gravity;
		this.height = height;
		this.thickness = thickness;
	}
	public PhysicsBody(float height) {
		this.gravity = 0.3f;
		this.height = height;
		this.thickness = 0.3f;
	}
	
	private int voxelCollide(Vector3f position, Vector3f offset) {
		int bid;
		bid = world.getBlock((int)(position.x() - thickness), (int)position.y(), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(-thickness, 0, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() - thickness), (int)position.y(), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(-thickness, 0, thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)position.y(), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(thickness, 0, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)position.y(), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(thickness, 0, thickness); return bid; }
		
		bid = world.getBlock((int)(position.x() - thickness), (int)(position.y() + height), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(-thickness, height, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() - thickness), (int)(position.y() + height), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(-thickness, height, thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)(position.y() + height), (int)(position.z() - thickness));
		if (bid > 0) { offset.set(thickness, height, -thickness); return bid; }
		bid = world.getBlock((int)(position.x() + thickness), (int)(position.y() + height), (int)(position.z() + thickness));
		if (bid > 0) { offset.set(thickness, height, thickness); return bid; }
		
		return 0;
	}
	
	private float handleCollision(int attr, float velocity) {
		nextPosition.set(position).setComponent(attr, position.get(attr) + velocity);
		
		int bid = voxelCollide(nextPosition, tmp);
		if (bid > 0 && world.blocks().get(bid - 1).translucent == BlockType.FLUID) {
			inWater = onGround = true;
		} else if (bid > 0) {
			float offset = tmp.get(attr);
			float p = Math.floor(position.get(attr) + offset);
			float np = Math.floor(nextPosition.get(attr) + offset);
			float invSignum  = p - np;
			float difference = invSignum * velocity;
			if (attr == 1 && invSignum > 0)
				onGround = true;
			
			velocity -= invSignum * difference;
		}
		
		return velocity;
	}
	
	private void updatePosition() {
		prevPosition.set(position);
		// position.add(velocity, nextPosition);
		onGround = inWater = false;
		
		// velocity.mul(1, 0, 0, tmpVelocity);
		if (!noclip) {
			if (velocity.x != 0) velocity.x = handleCollision(0, velocity.x); 
			if (velocity.y != 0) velocity.y = handleCollision(1, velocity.y); 
			if (velocity.z != 0) velocity.z = handleCollision(2, velocity.z); 
		}
		
		position.add(velocity.mul(inWater ? 0.75f : 1, tmp));
	}
	
	public void setFixedFramerate(float framerate) {
		fixedFramerate = 1f / framerate;
	}
	
	public void teleport(float x, float y, float z) {
		position.set(x, y, z);
		prevPosition.set(position);
		velocity.zero();
	}
	public void teleport(Vector3f pos) {
		position.set(pos);
		prevPosition.set(pos);
		velocity.zero();
	}
	
	public void setNoclipMode(boolean f) {
		noclip = world == null || f;
	}
	public boolean isOnGround() { return onGround; }
	public boolean isInWater() { return inWater; }
	public boolean noclip() { return noclip; }
	public Vector3f velocity() { return velocity; }
	public Vector3f position() { return position; }
	public World world() { return world; }
	
	public void setVelocityX(float x) { velocity.x = x * fixedFramerate; }
	public void setVelocityY(float y) {
		if (y > 0) onGround = false;
		velocity.y = y * fixedFramerate;
	}
	public void setVelocityZ(float z) { velocity.z = z * fixedFramerate; }
	
	public void update() {
		if (!(onGround || noclip || inWater)) velocity.sub(0, gravity * fixedFramerate, 0);
		updatePosition();
	}
}
