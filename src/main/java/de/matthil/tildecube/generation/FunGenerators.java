package de.matthil.tildecube.generation;

import de.matthil.tildecube.world.World;

import org.joml.SimplexNoise;

import static org.lwjgl.glfw.GLFW.glfwGetTime;

public class FunGenerators {
	public static void flatland(World world) {
		int x, y, z;
		for (int i = 0; i < world.x()*5*world.z(); ++i) {
			x = i % world.x();
			z = (i / world.x()) % world.z();
			y = i / (world.x() * world.z());
			
			int bid = y < 4 ? 3 : 2;
			world.usetBlock(x, y, z, bid);
		}
	}
	
	public static void perlinTerrain(World world) {
		Perlin perlin = new Perlin((long)glfwGetTime(), Math.max(world.x(), world.z()));
		
		final int width = world.x(), length = world.z();
		final float terrHeight = 15, terrTreshold = 15;
		int x, z;
		for (int i = 0; i < width*length; ++i) {
			x = i % width;
			z = i / width;
			float n = perlin.noise(x, z, .01f, 4);
			int y = (int)((n + 1f) * terrHeight) + 5;
			for (int j = 0; j < y; ++j) {
				// chunk.setBlock(x, j, z, (j == y - 1) ? 2 : 3);
				int bid = 1;
				if (j == y - 1 && y > 5 + terrTreshold)
					bid = 2;
				else if (j >= 7 + terrTreshold)
					bid = 3;
				else if (j > 5)
					bid = 12;
				world.usetBlock(x, j, z, bid);
			}
			for (int j = y; j < 4 + terrTreshold; ++j)
				world.usetBlock(x, j, z, 8);
		}
		
	}

	// Simplex caves
	public static void caves(World world) {
		float offX = (float)Math.random() * 10000f,
			  offY = (float)Math.random() * 10000f;
		
		final int width = world.x(), height = world.y(), length = world.z();
		final float frequency = 0.1f;
		int x, y, z; float nx, ny, nz;
		for (int i = 0; i < width*height*length; ++i) {
			x = i % width;
			z = (i / width) % length;
			y = i / (width * length);
			
			nx = (x + offX) * frequency;
			ny = (y + offY) * frequency;
			nz = z * frequency;
			
			float n = SimplexNoise.noise(nx, ny, nz);
			int bid = 0;
			if (n > -0.3f) bid = 1;
			if (y == height - 1) bid *= 2;
			
			world.usetBlock(x, y, z, bid);
		}
	}
}
