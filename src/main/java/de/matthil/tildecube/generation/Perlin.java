package de.matthil.tildecube.generation;

import org.joml.Math;
import org.joml.Random;

public class Perlin {
	public long seed;
	private int SIZE = 256;
	private int[] perm;
	
	public Perlin(long seed, int size) {
		this.SIZE = size;
		perm = new int[SIZE];
		seed(seed);
	}
	
	public void seed(long seed) {
		this.seed = seed;
		generatePermutationTable(seed);
	}
	
	private float fade(float t) {
		return ((6*t - 15)*t + 10)*t*t*t;
	}
	
	private int P(int idx) {
		return perm[idx % SIZE];
	}
	
	private float perlinDot(float vx, float vy, int val) {
		// Doing a little optimization
		int x = 1 - (val & 2), y = 1 - ((val & 1) << 1);
		
		return vx*(float)x + vy*(float)y;
	}
	
	public float perlin(float x, float y) {
		int X = (int)x % SIZE,
			Y = (int)y % SIZE;
		float xf = x % 1f,
			  yf = y % 1f;
		
		int   topLeft     = P(P(X) + Y+1),
		      topRight    = P(P(X+1) + Y+1),
		      bottomLeft  = P(P(X) + Y),
		      bottomRight = P(P(X+1) + Y);
		
		float dotTopLeft     = perlinDot(xf, yf - 1, topLeft),
		      dotTopRight    = perlinDot(xf - 1, yf - 1, topRight),
		      dotBottomLeft  = perlinDot(xf, yf, bottomLeft),
		      dotBottomRight = perlinDot(xf - 1, yf, bottomRight);
		
		float u = fade(xf), v = fade(yf);
		float result = Math.lerp(
				Math.lerp(dotBottomLeft, dotTopLeft, v),
				Math.lerp(dotBottomRight, dotTopRight, v),
				u
		);
		
		return result;
	}
	
	public float noise(float x, float y, float baseFreq, int octaves) {
		float frequency = baseFreq,
			  result = 0;
		float amplitude = 1;
		
		for (int i = 0; i < octaves; ++i) {
			result += amplitude * perlin(x * frequency, y * frequency);
			amplitude *= 0.5f;
			frequency *= 2;
		}
		
		return result;
	}
	
	private void generatePermutationTable(long seed) {
		int tmp, r;
		Random rnd = new Random(seed);
		for (int i = 0; i < SIZE; ++i) perm[i] = i;
		// Simple array shuffle
		for (int i = 0; i < SIZE; ++i) {
			tmp = perm[i];
			r = rnd.nextInt(SIZE);
			perm[i] = r;
			perm[r] = tmp;
		}
	}
}
