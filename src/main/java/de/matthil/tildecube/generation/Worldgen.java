package de.matthil.tildecube.generation;

import de.matthil.tildecube.world.World;

import org.joml.Math;

// The "ultimate" world generator
public class Worldgen {
	public static final float FREQUENCY = 0.05f;
	public static final int NOISE_GAP = 5;
	private static final int[] TREE_STRUCTURE = { 0,0,0,0,0,0,0,0,0,0,0,0,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,17,0,0,0,0,0,0,0,0,0,0,0,0,0,18,18,18,0,18,18,18,18,18,18,18,17,18,18,18,18,18,18,18,0,18,18,18,0,0,18,18,18,0,18,18,18,18,18,18,18,17,18,18,18,18,18,18,18,0,18,18,18,0,0,0,0,0,0,0,0,18,0,0,0,18,18,18,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0 };
	
	public long seed;
	public int width, height, length;
	
	private Perlin noise;
	private float[] heightmap, rivermap;
	
	public Worldgen(long seed, int w, int h, int l) {
		this.seed = seed;
		width = w;
		height = h;
		length = l;
		noise = new Perlin(seed, Math.max(w, l));
		
		heightmap = new float[w*l];
		rivermap  = new float[w*l];
		
		generateHeightmap(heightmap, FREQUENCY, 2);
		noise.seed(seed + 1);
		generateHeightmap(rivermap, FREQUENCY, 4);
	}
	
	private void generateHeightmap(float[] heightmap, float frequency, int octaves) {
		for (int i = 0; i < width*length; ++i) {
			float n = noise.noise(i % width, i / width, frequency, octaves);
			heightmap[i] = n;
		}
	}
	
	private int getHeight(int x, int z) {
		float h = heightmap[x+z*width];
		return (int)(h * NOISE_GAP + NOISE_GAP + (height >> 1));
	}
	
	// My attempt at making some kind of caving making algorithm
	private void digSphere(World world, int x, int y, int z, int radius) {
		int ix, iy, iz; boolean inSphere;
		int size = radius*2 + 2;
		for (int i = 0; i < size*size*size; ++i) {
			ix = i % size - (size >> 1);
			iy = i / (size * size) - (size >> 1);
			iz = (i / size) % size - (size >> 1);
			
			inSphere = ix*ix + iy*iy + iz*iz < radius*radius;
			ix += x; iy += y; iz += z;
			if (inSphere && world.getBlock(ix, iy, iz) == 1)
				world.usetBlock(ix, iy, iz, 0);
		}
	}
	
	private void craveCave(World world, int x, int y, int z, float amplitude) {
		// digSphere(world, (int)x, (int)y, (int)z, (int)amplitude);
		float ang = (float)Math.random() * 9999f;
		float dx = Math.cos(ang),
			  dy = -amplitude,
			  dz = Math.sin(ang),
			  da = (float)(Math.random() - 0.5) * 0.2f;
		
		final int length = 10 + (int)(Math.random() * 30);
		
		float ix = x, iy = y, iz = z;
		for (int i = 0; i < length; ++i) {
			ix += dx; iy += dy; iz += dz;
			digSphere(world, (int)ix, (int)iy, (int)iz, 3);
			
			ang += da;
			dx = Math.cos(ang); dz = Math.sin(ang);
			if (Math.random() < 0.1)
				da = (float)(Math.random() - 0.5) * 0.2f;
		}
	}
	
	private void craveCaves(World world, int count) {
		final int minHeight = 10, maxHeight = 28 - NOISE_GAP;
		for (int i = 0; i < count; ++i)
			craveCave(world,
					(int)(Math.random() * width),
					(int)(Math.random() * (maxHeight - minHeight) + minHeight),
					(int)(Math.random() * length), 0.2f);
	}
	
	private void spawnTree(World world, int tx, int ty, int tz) {
		int x, y, z, bid;
		for (int i = 0; i < TREE_STRUCTURE.length; ++i) {
			x = i % 5 - 2 + tx;
			y = i / 25 + ty;
			z = (i / 5) % 5 - 2 + tz;
			bid = TREE_STRUCTURE[i];
			
			if (bid != 0 && !(x < 0 || x > width || y > height || z < 0 || z > length))
				world.usetBlock(x, y, z, bid);
		}
	}
	
	private void spawnTrees(World world) {
		int w = width >> 4, h = height >> 4;
		int x, y, hm;
		for (int i = 0; i < w*h; ++i) {
			x = (i % w) * 16 + (int)(Math.random() * 15);
			y = (i / w) * 16 + (int)(Math.random() * 15);
			hm = getHeight(x, y);
			
			if (rivermap[x+y*width] > 0) {
				spawnTree(world, x, hm + 1, y);
				world.usetBlock(x, hm, y, 3);
			}
		}
	}
	
	private void waterFill(World world) {
		int waterLevel = (height + NOISE_GAP - 2) >> 1;
		for (int i = 0; i < width*length*waterLevel; ++i)
			world.usetBlock(i%width, i/(width*length), (i/width)%length, 8);
	}
	
	private void generateLandscape(World world) {
		int bid, x, z, h, j; boolean rivered;
		for (int i = 0; i < width*length; ++i) {
			x = i % width; z = i / width;
			h = getHeight(x, z);
			if (rivered = (rivermap[i] < 0)) h -= NOISE_GAP;
			
			for (j = h; j >= 0; --j) {
				if (!rivered && j == h)
					bid = 2;
				else if (j > h - 3)
					bid = rivered ? 12 : 3;
				else if (j == 0)
					bid = 7;
				else
					bid = 1;
				
				world.usetBlock(x, j, z, bid);
			}
		}
	}
	
	public void generate(World world) {
		world.resize(width >> 5, height >> 5, length >> 5);
		
		waterFill(world);
		generateLandscape(world);
		spawnTrees(world);
		// craveCave(world, 64, 24, 64, 0.2f);
		craveCaves(world, (width * height) >> 7);
	}
}
