package de.matthil.tildecube.handling;

import de.matthil.tildecube.Tildecube;

public interface EventHandler {
	public void update(Tildecube game, float dt);
	public void tick(Tildecube game, float dt);
	public void chat(Tildecube game, String msg);
	public void start(Tildecube game);
	// int[] info = { x, y, z, oldBid, newBid }
	public void block(Tildecube game, int[] info);
}
