package de.matthil.tildecube.handling;

import java.util.HashMap;

import de.matthil.tildecube.Tildecube;

public class EventManager {
	private HashMap<String, EventHandler> handlers;
	
	public EventManager() {
		handlers = new HashMap<String, EventHandler>();
	}
	
	public EventHandler getEventByName(String name) {
		return handlers.get(name);
	}
	public void pushEvent(String name, EventHandler event) {
		handlers.put(name, event);
	}
	public void deleteEvent(String name) {
		handlers.remove(name);
	}
	
	private void crash(Tildecube game, String name, Exception e) {
		System.err.println(name + " HAS FAILED!");
		game.chat.message("Handler '" + name + "' has failed. Check console for details.");
		e.printStackTrace();
		handlers.remove(name);
	}
	
	public void update(Tildecube game, float dt) {
		for (String key : handlers.keySet()) {
			try { handlers.get(key).update(game, dt); }
			catch (Exception e) {
				crash(game, key, e);
			}
		}
	}
	public void tick(Tildecube game, float dt) {
		for (String key : handlers.keySet()) {
			try { handlers.get(key).tick(game, dt); }
			catch (Exception e) {
				crash(game, key, e);
			}
		}
	}
	public void chat(Tildecube game, String msg) {
		for (String key : handlers.keySet()) {
			try { handlers.get(key).chat(game, msg); }
			catch (Exception e) {
				crash(game, key, e);
			}
		}
	}
	public void block(Tildecube game, int[] info) {
		for (String key : handlers.keySet()) {
			try { handlers.get(key).block(game, info); }
			catch (Exception e) {
				crash(game, key, e);
			}
		}
	}
}
