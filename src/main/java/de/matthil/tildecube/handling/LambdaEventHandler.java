package de.matthil.tildecube.handling;

import de.matthil.tildecube.Tildecube;

import java.util.function.Consumer;
import java.util.function.BiConsumer;

// Helper function for scripting
public class LambdaEventHandler implements EventHandler {
	private BiConsumer<Tildecube, Float> tick = null, update = null;
	private BiConsumer<Tildecube, String> chat = null;
	private BiConsumer<Tildecube, int[]> block = null;
	private Consumer<Tildecube> start = null;

	public LambdaEventHandler() { }
	
	public void setOnTick(BiConsumer<Tildecube, Float> tick) { this.tick = tick; }
	public void setOnUpdate(BiConsumer<Tildecube, Float> update) { this.update = update; }
	public void setOnStart(Consumer<Tildecube> start) { this.start = start; }
	public void setOnChat(BiConsumer<Tildecube, String> chat) { this.chat = chat; }
	public void setOnBlock(BiConsumer<Tildecube, int[]> block) { this.block = block; }
	
	@Override
	public void start(Tildecube game) {
		if (start != null) start.accept(game);
	}
	@Override
	public void tick(Tildecube game, float dt) {
		if (tick != null) tick.accept(game, dt);
	}
	@Override
	public void update(Tildecube game, float dt) {
		if (update != null) update.accept(game, dt);
	}
	@Override
	public void chat(Tildecube game, String msg) {
		if (chat != null) chat.accept(game, msg);
	}
	@Override
	public void block(Tildecube game, int[] info) {
		if (block != null) block.accept(game, info);
	}
}
