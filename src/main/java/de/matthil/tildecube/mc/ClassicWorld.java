package de.matthil.tildecube.mc;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.nio.ByteBuffer;

import de.matthil.tildecube.SimpleEntity;
import de.matthil.tildecube.world.*;

import net.querz.nbt.io.NBTUtil;
import net.querz.nbt.tag.*;
import net.querz.nbt.io.*;

// ClassicWorld format parser
// The format will be extended to allow the use of custom blocks.
public class ClassicWorld {
	public static void loadWorld(World world, SimpleEntity player, String filename) throws IOException {
		NamedTag tag = NBTUtil.read(filename);
		CompoundTag data = (CompoundTag)tag.getTag();
		
		int w = data.getShort("X"),
			h = data.getShort("Y"),
			l = data.getShort("Z");
		world.resize((int)Math.ceil(w / 32f),
					 (int)Math.ceil(h / 32f),
					 (int)Math.ceil(l / 32f));
		
		CompoundTag spawn = data.getCompoundTag("Spawn");
		float px = (float)spawn.getShort("X") / 32f,
			  py = (float)spawn.getShort("Y") / 32f,
			  pz = (float)spawn.getShort("Z") / 32f;
		
		byte[] blocks = data.getByteArray("BlockArray");
		int x, y, z;
		for (int i = 0; i < blocks.length; ++i) {
			x = i % w;
			y = i / (w * l);
			z = (i / w) % l;
			world.usetBlock(x, y, z, blocks[i]);
		}
		player.teleport(px, py, pz);
	}
	
	private static byte[] uuid2bytes(UUID uuid) {
		ByteBuffer buf = ByteBuffer.allocate(16);
		buf.putLong(uuid.getMostSignificantBits())
		   .putLong(uuid.getLeastSignificantBits());
		return buf.flip().array();
	}

	private static CompoundTag saveCustomBlocks(World world) {
		Block b;
		CompoundTag result = new CompoundTag();
		for (int i = 49; i < world.blocks().size(); ++i) {
			b = world.blocks().get(i);
			result.putByte(b.name, (byte)(i + 1));
		}

		return result;
	}
	
	public static void saveWorld(World world, SimpleEntity player, String filename) throws IOException {
		CompoundTag data = new CompoundTag();
		UUID uuid = UUID.randomUUID();
		byte[] uuidBytes = uuid2bytes(uuid);

		//// Basic metadata
		data.putByte("FormatVersion", (byte)1);
		data.putString("Name", filename);
		data.putByteArray("UUID", uuidBytes);

		//// World size
		data.putShort("X", (short)world.x());
		data.putShort("Y", (short)world.y());
		data.putShort("Z", (short)world.z());

		//// Spawn size
		CompoundTag spawn = new CompoundTag();
		spawn.putShort("X", (short)(player.position().x() * 32f));
		spawn.putShort("Y", (short)(player.position().y() * 32f));
		spawn.putShort("Z", (short)(player.position().z() * 32f));
		spawn.putByte("H", (byte) 0);
		spawn.putByte("P", (byte) 0);
		data.put("Spawn", spawn);

		//// Block map
		byte[] blocks = new byte[world.x()*world.y()*world.z()];
		for (int i = 0; i < blocks.length; ++i)
			blocks[i] = (byte)world.ugetBlock(i % world.x(), i / (world.x() * world.z()), (i / world.x()) % world.z());
		data.putByteArray("BlockArray", blocks);

		//// Tildecube-specific stuff
		CompoundTag metadata = new CompoundTag();
		data.put("TC_CustomBlocks", saveCustomBlocks(world));

		data.put("Metadata", metadata);

		//// Save file
		File file = new File(filename);
		file.delete();
		NBTUtil.write(data, file);
		
	}
}
