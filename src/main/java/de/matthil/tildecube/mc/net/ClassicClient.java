package de.matthil.tildecube.mc.net;

import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.io.*;

import de.matthil.tildecube.SimpleEntity;
import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.world.World;
import de.matthil.tildecube.mc.net.packets.*;

// netcooc
public class ClassicClient {
	public static byte[] encodeString(String str) {
		byte[] strBytes = str.getBytes(StandardCharsets.US_ASCII);
		byte[] result = new byte[64];
		int i = 0;
		for (; i < strBytes.length; ++i) result[i] = strBytes[i];
		for (; i < 64; ++i) result[i] = 0;
		
		return result;
	}
	public static String decodeString(DataInputStream stream) throws IOException {
		byte[] buffer = new byte[64];
		stream.readFully(buffer);
		
		return decodeString(buffer);
	}
	public static String decodeString(byte[] strBytes) {
		return new String(strBytes, StandardCharsets.US_ASCII).trim();
	}
	
	//// Non-static stuff
	
	private Socket socket = null;
	private DataInputStream inStream;
	private DataOutputStream outStream;
	
	private byte[] mapBuffer;
	public void initMapBuffer() {
		mapBuffer = new byte[0];
	}
	public void addToMapBuffer(byte[] data, int length) {
		int oldLength = mapBuffer.length;
		mapBuffer = Arrays.copyOf(mapBuffer, oldLength + length);
		System.arraycopy(data, 0, mapBuffer, oldLength, length);
	}
	public void decodeMapBuffer(Tildecube game, int x, int y, int z) throws IOException {
		byte[] writeBuffer = new byte[8192]; int readLen;
		World world = game.world;
		GZIPInputStream gzipStream = new GZIPInputStream(new ByteArrayInputStream(mapBuffer));
		DataInputStream dataStream = new DataInputStream(gzipStream);
		
		dataStream.readInt();
		world.resize((x >> 5) + 1, (y >> 5) + 1, (z >> 5) + 1);
		int counter = 0, idx;
		while ((readLen = dataStream.read(writeBuffer)) > 0 && counter < x*y*z) {
			for (int i = 0; i < readLen; ++i) {
				idx = i + counter;
				world.usetBlock(idx %x, idx / (x * z), (idx / x) % z, writeBuffer[i]);
			}
			counter += readLen;
		}
	}
	
	// Returns false when no packet
	public boolean handlePacket(Tildecube game) throws IOException {
		if (socket == null) return false;
		if (inStream.available() == 0) return false;
		Packet packet = null;
		
		int pid;
		switch (pid = inStream.readByte()) {
		case 0x0: packet = new P00ServerId(); break;
		case 0x1: break; // skip ping
		case 0x2: packet = new P02LevelInit(); break;
		case 0x3: packet = new P03LevelDataChunk(); break;
		case 0x4: packet = new P04LevelFinalize(); break;
		case 0x6: packet = new P0506SetBlock(); break;
		case 0x7: packet = new P07SpawnPlayer(); break;
		case 0x8: packet = new P08SetPosition(); break;
		case 0x9: packet = new P09UpdatePosOri(); break;
		case 0xa: packet = new P0AUpdatePos(); break;
		case 0xb: packet = new P0BUpdateOri(); break;
		case 0xc: packet = new P0CDespawnPlayer(); break;
		case 0xd: packet = new P0DMessage(); break;
		case 0xe: packet = new P0EDisconnect(); break;
		case 0xf: inStream.readByte(); break;
		case 0x10: packet = new P10ExtInfo(); break;
		case 0x11: packet = new P11ExtEntry(); break;
		default: throw new RuntimeException(String.format("Invalid network packet ID: %d", pid));
		}
		if (packet != null) {
			packet.readPacket(inStream);
			packet.packetReceived(game, this);
		}
		
		return true;
	}
	
	public void handleExtInfo(Tildecube game, P10ExtInfo info) {
		try {
			for (int i = 0; i < info.extCount; ++i)
				handlePacket(game);
			
			extInfo("Tildecube");
		} catch (IOException e) {
			game.crash(e);
		}
	}
	
	public void extInfo(String name) throws IOException{
		outStream.writeByte(0x10);
		new P10ExtInfo(name, 0).writePacket(outStream);
	}
	public void auth(String name, String mppass) throws IOException {
		outStream.writeByte(0x0);
		new P00ServerId(name, mppass).writePacket(outStream);
	}
	public void setBlock(int x, int y, int z, int bid) throws IOException {
		outStream.writeByte(0x5);
		new P0506SetBlock(x, y, z, bid).writePacket(outStream);
	}
	public void move(SimpleEntity player) throws IOException {
		float x = player.position().x(),
			  y = player.position().y(),
			  z = player.position().z(),
			  pitch = player.camera().pitch(),
			  yaw = player.camera().yaw();
		move(x, y, z, pitch, yaw);
	}
	public void move(float px, float py, float pz, float ppitch, float pyaw) throws IOException {
		int x = (int)(px * 32f),
			y = (int)(py * 32f + 51),
			z = (int)(pz * 32f),
			pitch = (int)((-ppitch / 360f) * 255),
			yaw = (int)(((pyaw + 90f) / 360f) * 255);
		
		outStream.writeByte(0x8);
		new P08SetPosition(x, y, z, pitch, yaw).writePacket(outStream);
	}
	public void message(String msg) throws IOException {
		outStream.writeByte(0xd);
		new P0DMessage(msg).writePacket(outStream);
	}
	
	public ClassicClient() { }
	
	public boolean isConnected() {
		return socket != null && socket.isConnected();
	}
	public void disconnect() {
		if (isConnected()) {
			try {
				socket.close();
			} catch (IOException e) { }
		}
		socket = null;
	}
	public void connect(String addr, int port, String name, String mppass) throws IOException {
		if (socket != null) disconnect();
		socket = new Socket(addr, port);
		
		inStream  = new DataInputStream(socket.getInputStream());
		outStream = new DataOutputStream(socket.getOutputStream());
		
		auth(name, mppass);
	}
}
