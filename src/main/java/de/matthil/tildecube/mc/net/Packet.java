package de.matthil.tildecube.mc.net;

import java.io.*;

import de.matthil.tildecube.Tildecube;

public interface Packet {
	public void readPacket(DataInputStream stream) throws IOException;
	public void writePacket(DataOutputStream stream) throws IOException;
	
	public void packetReceived(Tildecube game, ClassicClient client);
}
