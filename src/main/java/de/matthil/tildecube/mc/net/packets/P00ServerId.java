package de.matthil.tildecube.mc.net.packets;

import java.io.*;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P00ServerId implements Packet {
	public String name, key;
	
	public P00ServerId() { }
	public P00ServerId(String name, String mppass) {
		this.name = name;
		this.key  = mppass;
	}

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		stream.readByte();
		name = ClassicClient.decodeString(stream);
		key  = ClassicClient.decodeString(stream);
		stream.readByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
		stream.writeByte(0x7);
		stream.write(ClassicClient.encodeString(name));
		stream.write(ClassicClient.encodeString(key));
		stream.write(0x42);
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		System.out.printf("Connected as %s. MOTD: %s\n", name, key);
	}

}
