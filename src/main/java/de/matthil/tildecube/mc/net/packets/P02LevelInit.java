package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

import de.matthil.tildecube.ui.Menu;
import de.matthil.tildecube.ui.widgets.Label;

public class P02LevelInit implements Packet {

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		Menu loading = new Menu(game.guiAtlas);
		loading.canEscape(false);
		loading.pausesTheGame(false);
		loading.addWidget(new Label(0, "Loading world. Please wait."));
		
		game.openGUI(loading);
		client.initMapBuffer();
	}

}
