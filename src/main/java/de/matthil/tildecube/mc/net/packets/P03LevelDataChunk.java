package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P03LevelDataChunk implements Packet {
	public byte[] chunk = new byte[1024];
	public int chunkLength;

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		chunkLength = stream.readShort();
		stream.readFully(chunk);
		stream.readByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		client.addToMapBuffer(chunk, chunkLength);
	}

}
