package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P04LevelFinalize implements Packet {
	public int x, y, z;

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		x = stream.readShort();
		y = stream.readShort();
		z = stream.readShort();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		try {
			client.decodeMapBuffer(game, x, y, z);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		game.openGUI(null);
	}

}
