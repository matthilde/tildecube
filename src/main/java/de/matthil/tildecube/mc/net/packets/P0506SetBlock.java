package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P0506SetBlock implements Packet {
	public int x, y, z, bid;
	
	public P0506SetBlock() {}
	public P0506SetBlock(int x, int y, int z, int bid) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.bid = bid;
	}

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		x = stream.readShort();
		y = stream.readShort();
		z = stream.readShort();
		bid = stream.readByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
		stream.writeShort(x);
		stream.writeShort(y);
		stream.writeShort(z);
		stream.writeByte(1);
		stream.writeByte(bid);
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		game.world.setBlock(x, y, z, bid);
	}

}
