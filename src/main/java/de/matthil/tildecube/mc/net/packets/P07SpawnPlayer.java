package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P07SpawnPlayer implements Packet {
	public int x, y, z, id;
	public int pitch, yaw;
	public String name;

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		id    = stream.readUnsignedByte();
		name  = ClassicClient.decodeString(stream);
		x     = stream.readShort();
		y     = stream.readShort();
		z     = stream.readShort();
		yaw   = stream.readByte();
		pitch = stream.readByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		if (id == 0xff) return;
		game.spawnNetPlayer(id);
	}

}
