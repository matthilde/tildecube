package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.SimpleEntity;
import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P08SetPosition implements Packet {
	public int x, y, z, id;
	public int pitch, yaw;

	public P08SetPosition() { }
	public P08SetPosition(int x, int y, int z, int pitch, int yaw) {
		id = 0xff;
		this.x = x;
		this.y = y;
		this.z = z;
		this.pitch = pitch;
		this.yaw = yaw;
	}
	
	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		id    = stream.readUnsignedByte();
		x     = stream.readShort();
		y     = stream.readShort();
		z     = stream.readShort();
		yaw   = stream.readByte();
		pitch = stream.readByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
		stream.writeByte(id);
		stream.writeShort(x);
		stream.writeShort(y);
		stream.writeShort(z);
		stream.writeByte(yaw);
		stream.writeByte(pitch);
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		SimpleEntity ent = id == 0xff ? game.player : game.netPlayers()[id];
		ent.teleport((float)x / 32f, (float)y / 32f, (float)z / 32f);
	}

}
