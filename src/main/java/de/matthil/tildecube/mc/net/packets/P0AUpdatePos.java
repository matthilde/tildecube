package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.SimpleEntity;
import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P0AUpdatePos implements Packet {
	public int x, y, z, id;

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		id    = stream.readUnsignedByte();
		x     = stream.readByte();
		y     = stream.readByte();
		z     = stream.readByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		SimpleEntity ent = id == 0xff ? game.player : game.netPlayers()[id];
		ent.position().add((float)x / 32f, (float)y / 32f, (float)z / 32f);
	}

}
