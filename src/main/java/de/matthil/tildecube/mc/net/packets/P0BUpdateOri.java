package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P0BUpdateOri implements Packet {
	public int id;
	public int pitch, yaw;

	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		id    = stream.readUnsignedByte();
		yaw   = stream.readByte();
		pitch = stream.readByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
	}

}
