package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P0CDespawnPlayer implements Packet {
	public int pid;
	
	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		pid = stream.readUnsignedByte();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException { }

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		game.deleteNetPlayer(pid);
	}

}
