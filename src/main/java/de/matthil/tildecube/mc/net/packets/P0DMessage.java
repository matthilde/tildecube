package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P0DMessage implements Packet {
	String message;

	private static String sanitize(String msg) {
		return msg.replaceAll("&.", "");
	}
	
	public P0DMessage() { }
	public P0DMessage(String msg) { message = msg; }
	
	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		stream.readByte();
		message = ClassicClient.decodeString(stream);
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
		byte[] buf = ClassicClient.encodeString(message);
		stream.writeByte(0xff);
		stream.write(buf);
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		game.chat.message(sanitize(message));
	}

}
