package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;
import de.matthil.tildecube.ui.Menu;
import de.matthil.tildecube.ui.widgets.Label;

public class P0EDisconnect implements Packet {
	String reason;
	
	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		reason = ClassicClient.decodeString(stream);
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException { }

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		Menu disconnect = new Menu(game.guiAtlas);
		disconnect.canEscape(false);
		disconnect.pausesTheGame(true);
		
		disconnect.addWidget(new Label(-50, "Disconnected!"));
		disconnect.addWidget(new Label(50, reason));
		
		game.openGUI(disconnect);
		client.disconnect();
	}

}
