package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P10ExtInfo implements Packet {
	public String name;
	public int extCount;
	
	public P10ExtInfo() { }
	public P10ExtInfo(String name, int extCount) {
		this.name = name;
		this.extCount = extCount;
	}
	
	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		name = ClassicClient.decodeString(stream);
		extCount = stream.readShort();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
		stream.write(ClassicClient.encodeString(name));
		stream.writeShort(extCount);
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) {
		client.handleExtInfo(game, this);
	}

}
