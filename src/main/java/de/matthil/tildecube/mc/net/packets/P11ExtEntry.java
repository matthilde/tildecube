package de.matthil.tildecube.mc.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import de.matthil.tildecube.Tildecube;
import de.matthil.tildecube.mc.net.ClassicClient;
import de.matthil.tildecube.mc.net.Packet;

public class P11ExtEntry implements Packet {
	public String name;
	public int version;
	
	public P11ExtEntry() { }
	public P11ExtEntry(String name, int version) {
		this.name = name;
		this.version = version;
	}
	
	@Override
	public void readPacket(DataInputStream stream) throws IOException {
		name = ClassicClient.decodeString(stream);
		version = stream.readInt();
	}

	@Override
	public void writePacket(DataOutputStream stream) throws IOException {
		stream.write(ClassicClient.encodeString(name));
		stream.writeInt(version);
	}

	@Override
	public void packetReceived(Tildecube game, ClassicClient client) { }

}
