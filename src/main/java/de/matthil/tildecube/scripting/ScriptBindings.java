package de.matthil.tildecube.scripting;

import de.matthil.tildecube.*;
import de.matthil.tildecube.handling.*;

import java.util.List;
import java.util.ArrayList;

// Collection of functions to let the user write scripts easily
// An instance of this class is assigned to the global G
public class ScriptBindings {
	private Tildecube game;
	private ArrayList<Integer> chunksToUpdate = new ArrayList<Integer>(256);
	
	public ScriptBindings(Tildecube game) {
		this.game = game;
	}
	
	// Eventing helper functions
	public LambdaEventHandler newEventHandler() {
		// Avoids the use of imports and classes
		// Makes more concise syntax
		return new LambdaEventHandler();
	}
	public void addEvent(String name, EventHandler handler) {
		game.addEventHandler(name, handler);
	}
	public void deleteEvent(String name) {
		game.removeEventHandler(name);
	}
	
	// World Operations
	public void setBlock(int x, int y, int z, int bid) {
		game.setBlock(x, y, z, bid, true);
	}
	public int getBlock(int x, int y, int z) {
		return game.getBlock(x, y, z);
	}
	public int[] getWorldSize() {
		return new int[] {
			game.world.x(),
			game.world.y(),
			game.world.z()
		};
	}
	
	// Block Management
	// System to efficiently set mutliple blocks at once
	/*
	 * Example usage:
	 * 		G.bmBegin()
	 * 		[ subsequent G.bmSetBlock()/G.bmSetBlocks() calls ]
	 * 		G.bmEnd()
	 */
	public void bmBegin() {
		if (game.multiplayer != null)
			throw new RuntimeException("bm functions are not available in multiplayer.");
		chunksToUpdate.clear();
	}
	public void bmEnd() {
		for (int idx : chunksToUpdate)
			game.world.updateChunkMesh(idx);
	}
	private void bmCheckBlock(int x, int y, int z, int bid) {
		// Need to check since it uses (fast but unsafe) usetBlock
		if (x < 0 || y < 0 || z < 0 || x >= game.world.x() || y >= game.world.y() || z >= game.world.z())
			return;
		int idx = game.world.getChunkIdx(x>>5, y>>5, z>>5);
		if (!chunksToUpdate.contains(idx))
			chunksToUpdate.add(idx);
		game.world.usetBlock(x, y, z, bid);
	}
	public void bmSetBlock(int x, int y, int z, int bid) {
		bmCheckBlock(x, y, z, bid);
	}
	public void bmSetBlocks(List<int[]> blocks) {
		for (int[] block : blocks)
			bmCheckBlock(block[0], block[1], block[2], block[3]);
	}
	
	// Player-related stuff
	public float[] getPlayerPosition() {
		float[] result = {
			game.player.position().x(),
			game.player.position().y(),
			game.player.position().z(),
		};
		return result;
	}
	public void teleport(float x, float y, float z) {
		game.player.teleport(x, y, z);
	}
	public void setPlayerNoclip(boolean noclip) {
		game.player.setNoclipMode(noclip);
	}
	
	// Chat
	public void print(Object msg) {
		game.chat.message(msg.toString());
	}
	public void clearChat() {
		game.chat.reset();
	}
	
	// Dead meme
	public void amogus() { game.spawnAmogus(); }
	
	// Camera
	public int[] getRaycast() {
		return game.getRaycastPosition();
	}
	public int[] getRaycastPlusNormal() {
		int[] pos = getRaycast();
		int[] normal = game.getRaycastNormal();
		if (pos == null || normal == null) return null;
		
		return new int[] {
			pos[0] + normal[0],
			pos[1] + normal[1],
			pos[2] + normal[2]
		};
	}
}
