package de.matthil.tildecube.scripting;

import de.matthil.tildecube.Tildecube;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import groovy.lang.*;

// jaaj scrrcs
public class ScriptEngine {
	String error = "";
	boolean hasError = false;
	
	Binding binding;
	GroovyShell shell;
	
	public ScriptEngine(Tildecube game) {
		binding = new Binding();
		binding.setProperty("game", game);
		binding.setProperty("G", new ScriptBindings(game));
		shell = new GroovyShell(binding);
	}
	
	public boolean hasError() {
		return hasError;
	}
	public String getError() {
		return error;
	}
	public String toString(Object o) {
		return o.toString();
	}
	public Object evalFromFile(String filename) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(filename));
		
		return eval(new String(encoded), filename);
	}
	public Object eval(String eval) {
		return eval(eval, "eval");
	}
	public Object eval(String eval, String name) {
		hasError = false;
		try {
			return shell.run(eval, name, new ArrayList<String>(0));
		} catch (Exception e) {
			error = e.getMessage();
			hasError = true;
			return null;
		}
	}
}
