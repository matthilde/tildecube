package de.matthil.tildecube.ui;

import java.util.ArrayList;

import de.matthil.glgraphics.Atlas;
import de.matthil.glgraphics.UserInterface;
import de.matthil.glgraphics.Window;
import de.matthil.tildecube.world.Block;

// I hate gui programming, but i have to.
public class BlockPicker implements UserInterface {
	//// Constants
	private static final int MENU_X = 10, MENU_Y = 5;
	private static final int SIZE_X = (8 + 18 * MENU_X - 2) * 2, SIZE_Y = (8 + 18 * MENU_Y - 2) * 2;
	
	//// Variables
	private Inventory inventory;
	private ArrayList<Block> blocks;
	private Atlas blockAtlas;
	private int selected = -1;
	
	public BlockPicker(Inventory inv, ArrayList<Block> blocks, Atlas blockAtlas) {
		this.blocks = blocks;
		this.blockAtlas = blockAtlas;
		this.inventory = inv;
	}

	@Override
	public void onStart() {}
	
	@Override
	public boolean canEscape() {
		return true;
	}

	@Override
	public boolean pausesTheGame() {
		return false;
	}

	@Override
	public boolean blockedKeys() {
		return true;
	}

	@Override
	public void onKeyPress(Window win, int key) { }
	@Override
	public void onChar(Window win, int c) { }

	@Override
	public void onMousePress(Window win, float x, float y, int key) {
		if (selected >= 0)
			inventory.handleMiddleClick(selected);
		// Close after click, does not matter if a block has been picked.
		win.openGUI(null);
	}
	
	@Override
	public void onMouseMove(Window win, float x, float y) {
		int px = ((int)win.width() - SIZE_X) / 2, py = ((int)win.height() - SIZE_Y) / 2;
		
		if (x >= px + 8 && y >= py + 8 && x <= px + SIZE_X - 8 && y <= py + SIZE_Y - 8) {
			int idx = (((int)x - px - 8) / 36) + (((int)y - py - 8) / 36) * MENU_X;
			selected = idx < blocks.size() ? idx : -1;
		} else selected = -1;
	}

	@Override
	public void draw(Window win) {
		int px = ((int)win.width() - SIZE_X) / 2, py = ((int)win.height() - SIZE_Y) / 2;
		win.drawRectangle(px, py, SIZE_X, SIZE_Y, 0, 0, 0, .5f);
		
		int x, y, offX, offY, id;
		for (int i = 0; i < blocks.size(); ++i) {
			x = i % MENU_X; y = i / MENU_X;
			offX = 8 + 36 * x; offY = 8 + 36 * y;
			id = blocks.get(i).forward;
			win.drawTextureFromAtlas(blockAtlas, id, px + offX, py + offY, 2);
		}
		if (selected >= 0)
			win.drawRectangle(px + 8 + 36 * (selected % MENU_X), py + 8 + 36 * (selected / MENU_X), 32, 32, 1, 1, 1, 0.5f);
	}

}
