package de.matthil.tildecube.ui;

import java.util.ArrayList;

import de.matthil.glgraphics.UserInterface;
import de.matthil.glgraphics.Window;
import de.matthil.tildecube.ui.chat.ChatMessageEvent;

import org.joml.Math;

import static org.lwjgl.glfw.GLFW.*;

public class Chat implements UserInterface {
	//// Constants
	public static final int CHAT_HEIGHT = 8;
	
	//// Variables
	private boolean skipFirst = true;
	private int cursor = 0;
	private ArrayList<String> chatBuffer = new ArrayList<String>(CHAT_HEIGHT);
	private String chatString;
	private StringBuilder inputBuffer;
	
	// Events;
	private ChatMessageEvent onUserMessage = null;
	
	public Chat() {
		updateChatString();
	}
	
	public void setOnUserMessageHandler(ChatMessageEvent event) {
		onUserMessage = event;
	}
	
	@Override
	public void onStart() {
		skipFirst = true;
		cursor = 0;
		inputBuffer = new StringBuilder();
	}
	
	private void updateChatString() {
		StringBuilder result = new StringBuilder();
		for (int i = Math.max(0, chatBuffer.size() - CHAT_HEIGHT); i < chatBuffer.size(); ++i)
			result.append(chatBuffer.get(i) + "\n");
		
		chatString = result.toString();
	}
	public void message(String msg) {
		for (String line : msg.trim().split("\n"))
			chatBuffer.add(line);
		updateChatString();
	}
	public void reset() {
		chatBuffer.clear();
		updateChatString();
	}

	@Override
	public boolean canEscape() { return true; }
	@Override
	public boolean pausesTheGame() { return false; }
	@Override
	public boolean blockedKeys() { return true; }

	@Override
	public void onKeyPress(Window window, int key) {
		switch (key) {
		case GLFW_KEY_ENTER:
			String msg = inputBuffer.toString();
			onUserMessage.run(this, msg);
			if (window.currentUI == this)
				window.openGUI(null);
			break;
		case GLFW_KEY_BACKSPACE:
			if (cursor > 0 && inputBuffer.length() > 0)
				inputBuffer.deleteCharAt(--cursor);
			break;
		case GLFW_KEY_LEFT:
			if (cursor > 0) cursor--;
			break;
		case GLFW_KEY_RIGHT:
			if (cursor < inputBuffer.length()) cursor++;
			break;
		}
	}
	@Override
	public void onChar(Window win, int c) { 
		if (skipFirst) {
			skipFirst = false;
			return; // skip T keystroke
		}
		// inputBuffer.append((char)c);
		inputBuffer.insert(cursor++, (char)c);
	}

	@Override
	public void onMousePress(Window window, float x, float y, int key) { }
	@Override
	public void onMouseMove(Window window, float x, float y) { }

	public void drawChatWindow(Window win) {
		int fontHeight = win.fontHeight();
		int y = (int)win.height() - (2 * fontHeight * (1 + Math.min(chatBuffer.size(), 8)));
		
		if (chatString.length() != 0)
			win.drawText(chatString, 4, y, 2);
	}
	
	private void drawInputWindow(Window win) {
		int y = (int)(win.height() - 2 * win.fontHeight() - 4);
		int x = 4 + win.fontWidth() * 2 * cursor;
		win.drawRectangle(4, y, 128 * win.fontWidth(), win.fontHeight() * 2, 0, 0, 0, 0.5f);
		win.drawText(inputBuffer, 4, y, 2);
		win.drawRectangle(x, y, win.fontWidth() * 2, win.fontHeight() * 2, 1, 1, 1, 0.7f);
	}
	
	@Override
	public void draw(Window win) {
		drawChatWindow(win);
		drawInputWindow(win);
	}

}
