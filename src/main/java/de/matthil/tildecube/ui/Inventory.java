package de.matthil.tildecube.ui;

import java.util.ArrayList;

import de.matthil.glgraphics.*;
import de.matthil.tildecube.world.Block;

public class Inventory {
	//// Constants
	// uv
	private final float[] INVENTORY_BAR_UV = {
		0, 0, 182f / 256f, 22f / 256f
		// 0, 0, 1, 1
	};
	private final float[] INVENTORY_CURSOR_UV = {
		0, 22f/256f, 24f/256f, 24f/256f
	};
	private final int INVENTORY_SIZE = 9;
	
	// anchors
	private final int[] SIZES = {
		182, 22,  // bar
		24, 24,   // cursor
		16, 16,   // tile
	};
	
	//// Variables
	private Atlas gui, terrain;
	private ArrayList<Block> blocks;
	private int[] inventoryData = { 0, 3, 44, 2, 4, 16, 17, 19, 42 };
	
	// Temporary variables
	float uv[] = new float[4];
	
	int pickedSlot = 0;
	
	public Inventory(Atlas gui, Atlas terrain, ArrayList<Block> blocks) {
		this.gui = gui;
		this.terrain = terrain;
		this.blocks = blocks;
	}
	
	//// Drawing the thing
	public void draw(Window win) {
		int px = (int)win.width() / 2 - SIZES[0], py = (int)win.height() - SIZES[1] * 2;
		int slotoffset = -2 + pickedSlot * 40;
		
		win.drawTextureEx(gui.texture(), px, py, SIZES[0], SIZES[1], 2, INVENTORY_BAR_UV);
		for (int i = 0; i < 9; ++i) {
			int data = blocks.get(inventoryData[i]).forward;
			if (data >= 0)
				win.drawTextureFromAtlas(terrain, data, px + 6 + i * 40, py + 6, 2);
		}
		win.drawTextureEx(gui.texture(), px + slotoffset, py - 2, SIZES[2], SIZES[3], 2, INVENTORY_CURSOR_UV);
	}
	
	//// Picking the right slot for your needs!
	public void nextSlot() { pickedSlot = (pickedSlot + 1) % INVENTORY_SIZE; }
	public void prevSlot() { pickedSlot = (pickedSlot + INVENTORY_SIZE - 1) % INVENTORY_SIZE; }
	public void pickSlot(int slot) { pickedSlot = slot % INVENTORY_SIZE; }
	
	public void handleMiddleClick(int bid) {
		for (int i = 0; i < inventoryData.length; ++i) {
			if (inventoryData[i] == bid) pickSlot(i);
		}
		modifyCurrentSlot(bid);
	}
	public void modifyCurrentSlot(int bid) { inventoryData[pickedSlot] = bid; }
	
	public int getCurrentBlock() { return inventoryData[pickedSlot]; }
}
