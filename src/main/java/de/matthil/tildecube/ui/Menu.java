package de.matthil.tildecube.ui;

import java.util.ArrayList;

import de.matthil.glgraphics.UserInterface;
import de.matthil.glgraphics.Window;
import de.matthil.glgraphics.Atlas;

import de.matthil.tildecube.ui.widgets.*;

public class Menu implements UserInterface {
	private boolean canEscape = true, pausesGame = true;
	private Atlas guiAtlas;
	private ArrayList<Widget> widgets = new ArrayList<Widget>();
	
	public Menu(Atlas guiAtlas) {
		this.guiAtlas = guiAtlas;
	}
	
	public void addWidget(Widget widget) {
		widgets.add(widget);
	}
	
	@Override
	public void onStart() {}
	
	
	@Override
	public boolean canEscape() {
		return canEscape;
	}
	public void canEscape(boolean b) { canEscape = b; }

	@Override
	public boolean pausesTheGame() {
		return pausesGame;
	}
	public void pausesTheGame(boolean b) { pausesGame = b; }

	@Override
	public boolean blockedKeys() {
		return true;
	}

	@Override
	public void onKeyPress(Window window, int key) { }
	@Override
	public void onChar(Window win, int c) { }

	@Override
	public void onMousePress(Window window, float x, float y, int key) {
		x -= window.width() / 2;
		y -= window.height() / 2;
		for (Widget w : widgets)
			w.onClick(window, x, y);
	}
	@Override
	public void onMouseMove(Window window, float x, float y) {
		x -= window.width() / 2;
		y -= window.height() / 2;
		for (Widget w : widgets)
			w.onMouseMove(window, x, y);
	}

	@Override
	public void draw(Window window) {
		float offsetX = window.width() / 2,
			  offsetY = window.height() / 2;
		
		window.drawRectangle(0, 0, (int)window.width(), (int)window.height(), 0, 0, 0, 0.5f);
		for (Widget w : widgets)
			w.draw(window, guiAtlas, offsetX, offsetY);
	}

}
