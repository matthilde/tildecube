package de.matthil.tildecube.ui.chat;

import de.matthil.tildecube.ui.Chat;

public interface ChatMessageEvent {
	public void run(Chat chat, String message);
}
