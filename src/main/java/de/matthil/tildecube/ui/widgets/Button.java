package de.matthil.tildecube.ui.widgets;

import de.matthil.glgraphics.Window;
import de.matthil.glgraphics.Atlas;

public class Button implements Widget {
	//// Constants
	static final public int[] BUTTON_SIZE = {
		200, 20
	};
	static final private float[] BUTTON_UV_SIZE = {
		(float)BUTTON_SIZE[0] / 256f, (float)BUTTON_SIZE[1] / 256f
	};
	static final private float[] UV_OFFSETS = {
		46f/256f, 66f/256f, 86f/256f // clicked, normal, hovered
	};
	
	//// Variables
	private WidgetEventHandler onClickHandler;
	private String label;
	private int y;
	private boolean hovering = false;

	public Button(int y, String label, WidgetEventHandler onClick) {
		this.label = label;
		this.onClickHandler = onClick;
		this.y = y;
	}
	
	@Override
	public boolean isHovering(float x, float y) {
		return x >= -BUTTON_SIZE[0] && y >= -BUTTON_SIZE[1] + this.y && x <= BUTTON_SIZE[0] && y <= BUTTON_SIZE[1] + this.y;
	}
	
	@Override
	public void onMouseMove(Window window, float x, float y) {
		hovering = isHovering(x, y);
	}

	@Override
	public void onClick(Window window, float x, float y) {
		if (hovering)
			onClickHandler.run(this, window);
	}

	@Override
	public void draw(Window window, Atlas guiAtlas, float offsetX, float offsetY) {
		int x = (int)(offsetX - BUTTON_SIZE[0]),
			y = (int)(offsetY - BUTTON_SIZE[1] + this.y);
		
		int fx = (int)(offsetX - window.fontWidth() * label.length()),
			fy = (int)(offsetY - window.fontHeight() + this.y + 2);
	
		float[] uv = { 0, UV_OFFSETS[hovering ? 2 : 1], BUTTON_UV_SIZE[0], BUTTON_UV_SIZE[1] };
		window.drawTextureEx(guiAtlas.texture(), x, y, BUTTON_SIZE[0], BUTTON_SIZE[1], 2, uv);
		window.drawText(label, fx, fy, 2);
	}

	@Override
	public void setLabel(String label) {
		this.label = label;
	}

}
