package de.matthil.tildecube.ui.widgets;

import de.matthil.glgraphics.Atlas;
import de.matthil.glgraphics.Window;

public class Label implements Widget {
	private String label;
	private int y;
	
	public Label(int y, String label) {
		this.label = label;
		this.y = y;
	}
	
	@Override
	public boolean isHovering(float x, float y) {
		return false;
	}

	@Override
	public void onClick(Window window, float x, float y) {}
	@Override
	public void onMouseMove(Window window, float x, float y) {}

	@Override
	public void draw(Window window, Atlas guiAtlas, float offsetX, float offsetY) {
		int fx = (int)(offsetX - window.fontWidth() * label.length()),
			fy = (int)(offsetY - window.fontHeight() + this.y);
	
		window.drawText(label, fx, fy, 2);
	}

	@Override
	public void setLabel(String label) {
		this.label = label;
	}

}
