package de.matthil.tildecube.ui.widgets;

import de.matthil.glgraphics.Window;
import de.matthil.glgraphics.Atlas;

public class SizedButton implements Widget {
	//// Constants
	public int[] BUTTON_SIZE = {
		200, 20
	};
	private float[] BUTTON_UV_SIZE = {
		(float)BUTTON_SIZE[0] / 256f, (float)BUTTON_SIZE[1] / 256f
	};
	private float[] UV_OFFSETS = {
		// Y
		46f/256f, 66f/256f, 86f/256f, // clicked, normal, hovered
		// X
		180f/256f // right end of the button
	};
	
	//// Variables
	private WidgetEventHandler onClickHandler;
	private String label;
	private int x, y, width;
	private boolean hovering = false;
	
	private void generateArrays() {
		BUTTON_SIZE[0] = width - 20;
		BUTTON_UV_SIZE[0] = (float)BUTTON_SIZE[0] / 256f;
	}

	public SizedButton(int x, int y, int width, String label, WidgetEventHandler onClick) {
		this.label = label;
		this.onClickHandler = onClick;
		this.x = x;
		this.y = y;
		this.width = width;
		generateArrays();
	}
	
	@Override
	public boolean isHovering(float x, float y) {
		return x >= -BUTTON_SIZE[0] + this.x && y >= -BUTTON_SIZE[1] + this.y && x <= BUTTON_SIZE[0] + this.x && y <= BUTTON_SIZE[1] + this.y;
	}
	
	@Override
	public void onMouseMove(Window window, float x, float y) {
		hovering = isHovering(x, y);
	}

	@Override
	public void onClick(Window window, float x, float y) {
		if (hovering)
			onClickHandler.run(this, window);
	}

	@Override
	public void draw(Window window, Atlas guiAtlas, float offsetX, float offsetY) {
		int x = (int)(offsetX - BUTTON_SIZE[0] - 20 + this.x),
			y = (int)(offsetY - BUTTON_SIZE[1] + this.y);
		
		int fx = (int)(offsetX - window.fontWidth() * label.length() + this.x),
			fy = (int)(offsetY - window.fontHeight() + this.y + 2);
	
		float[] uv = { 0, UV_OFFSETS[hovering ? 2 : 1], BUTTON_UV_SIZE[0], BUTTON_UV_SIZE[1] };
		float[] enduv = { UV_OFFSETS[3], UV_OFFSETS[hovering ? 2 : 1], BUTTON_UV_SIZE[1], BUTTON_UV_SIZE[1] };
		window.drawTextureEx(guiAtlas.texture(), x, y, BUTTON_SIZE[0], BUTTON_SIZE[1], 2, uv);
		window.drawTextureEx(guiAtlas.texture(), x + BUTTON_SIZE[0] * 2, y, BUTTON_SIZE[1], BUTTON_SIZE[1], 2, enduv);
		window.drawText(label, fx, fy, 2);
	}

	@Override
	public void setLabel(String label) {
		this.label = label;
	}

}
