package de.matthil.tildecube.ui.widgets;

import de.matthil.glgraphics.Window;
import de.matthil.glgraphics.Atlas;

public interface Widget {
	// Checks if the mouse is hovering the widget
	public boolean isHovering(float x, float y);
	// On click
	public void onClick(Window window, float x, float y);
	// On mouse move
	public void onMouseMove(Window window, float x, float y);
	// Drawing function
	public void draw(Window window, Atlas guiAtlas, float offsetX, float offsetY);
	// Label
	public void setLabel(String label);
}
