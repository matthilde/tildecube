package de.matthil.tildecube.ui.widgets;

import de.matthil.glgraphics.Window;

public interface WidgetEventHandler {
	void run(Widget self, Window window);
}
