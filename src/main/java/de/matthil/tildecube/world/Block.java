package de.matthil.tildecube.world;

import java.util.Arrays;
import java.util.ArrayList;

import de.matthil.tildecube.Tildecube;

public class Block {
	public BlockType translucent;
	public int up, down, left, right, forward, backward;
	public BlockEventHandler handler = null;
	public String name = null;
	
	public Block(BlockType translucent, int up, int down, int left, int right, int forward, int backward) {
		this.translucent = translucent;
		this.up = up;
		this.down = down;
		this.left = left;
		this.right = right;
		this.forward = forward;
		this.backward = backward;
	}
	public Block(BlockType translucent, int tex) {
		this.translucent = translucent;
		up = down = left = right = forward = backward = tex;
	}
	public Block(BlockType translucent, int up, int sides) {
		this.translucent = translucent;
		this.up = up;
		this.down = up;
		left = right = forward = backward = sides;
	}
	public Block(BlockType translucent, int up, int down, int sides) {
		this.translucent = translucent;
		this.up = up;
		this.down = down;
		left = right = forward = backward = sides;
	}

	// Naming blocks will allow Tildecube to recognize custom blocks.
	public void setName(String name) {
		this.name = name;
	}
	public void setEventHandler(BlockEventHandler handler) {
		this.handler = handler;
	}
	public void triggerEvent(Tildecube game, World world, int x, int y, int z, boolean destroy) {
		if (handler != null) this.handler.run(game, world, x, y, z, destroy);
	}
	
	public static ArrayList<Block> defaultBlocks() {
		Block[] blocks = {
			new Block(BlockType.SOLID, 1), // stone
			new Block(BlockType.SOLID, 0, 2, 3, 3, 3, 3), // grass
			new Block(BlockType.SOLID, 2), // dirt
			new Block(BlockType.SOLID, 16), // cobblestone
			new Block(BlockType.SOLID, 4), // woodplanks
			new Block(BlockType.PLANT, 15), // sapling
			new Block(BlockType.SOLID, 17), // bedrock
			new Block(BlockType.FLUID, 14), // water
			new Block(BlockType.FLUID, 14), // water
			new Block(BlockType.FLUID, 30), // lava
			new Block(BlockType.FLUID, 30), // lava
			new Block(BlockType.SOLID, 18), // sand
			new Block(BlockType.SOLID, 19), // gravel
			new Block(BlockType.SOLID, 32), // gold ore
			new Block(BlockType.SOLID, 33), // iron ore
			new Block(BlockType.SOLID, 34), // coal ore
			new Block(BlockType.SOLID, 21, 20), // log
			new Block(BlockType.LEAVES, 22), // leaves
			new Block(BlockType.SOLID, 48), // sponge
			new Block(BlockType.TRANSLUCENT, 49), // glass
			new Block(BlockType.SOLID, 64), // wool
			new Block(BlockType.SOLID, 65),
			new Block(BlockType.SOLID, 66),
			new Block(BlockType.SOLID, 67),
			new Block(BlockType.SOLID, 68),
			new Block(BlockType.SOLID, 69),
			new Block(BlockType.SOLID, 70),
			new Block(BlockType.SOLID, 71),
			new Block(BlockType.SOLID, 72),
			new Block(BlockType.SOLID, 73),
			new Block(BlockType.SOLID, 74),
			new Block(BlockType.SOLID, 75),
			new Block(BlockType.SOLID, 76),
			new Block(BlockType.SOLID, 77),
			new Block(BlockType.SOLID, 78),
			new Block(BlockType.SOLID, 79),
			new Block(BlockType.PLANT, 13), // flower
			new Block(BlockType.PLANT, 12), // flower
			new Block(BlockType.PLANT, 29), // mushroom
			new Block(BlockType.PLANT, 28), // mushroom
			new Block(BlockType.SOLID, 24), // gold block
			new Block(BlockType.SOLID, 23), // iron block
			new Block(BlockType.SOLID, 6, 5), // doubleslab
			new Block(BlockType.SOLID, 6), // supposedly one slab, but it's not
			new Block(BlockType.SOLID, 7), // bricks
			new Block(BlockType.SOLID, 9, 10, 8), // tnt
			new Block(BlockType.SOLID, 4, 35), // bookshelf
			new Block(BlockType.SOLID, 36), // mossy cobblestone
			new Block(BlockType.SOLID, 37), // obsidian
		};
		
		return new ArrayList<Block>(Arrays.asList(blocks));
	}
}
