package de.matthil.tildecube.world;

import de.matthil.tildecube.Tildecube;

public interface BlockEventHandler {
	public void run(Tildecube game, World world, int x, int y, int z, boolean destroy);
}
