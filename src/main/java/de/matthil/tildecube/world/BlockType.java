package de.matthil.tildecube.world;

public enum BlockType {
	SOLID, TRANSLUCENT, PLANT, LEAVES, FLUID
}
