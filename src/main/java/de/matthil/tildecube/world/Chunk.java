package de.matthil.tildecube.world;

import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.system.MemoryUtil.*;

import de.matthil.glgraphics.LinearAlloc;
import de.matthil.glgraphics.Shader;

/*
 * NOTE: This Chunk class is to be used internally.
 * 		 It has a lot of assumptions so I could make the code branchless.
 * 		 Due to its branchless nature, it's also a bit funky.
 *       So I commented encodeVertex()
 */
public class Chunk {
	//////// STATIC THINGS
	// DO NOT CHANGE! Specific vertex optimizations has been made
	// for this exact chunk size.
	public static final int CHUNK_SIZE = 32;
	public static final int CHUNK_FULL_SIZE = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;
	
	// Some parameter information used in pushFace()
	private static final int[] FACE_OFFSETS = {
		1, 2, 0, 0,
		0, 2, 1, 0,
		1, 0, 1, 0
	};
	private static final int[] LUMA_SHADOW = {
		4, 0, 2
	};
	private static final int[] UV_ORDER = {
		1, 0, 2, 3,
		3, 2, 0, 1
	};
	
	//////// NOT STATIC THINGS
	// pre-allocated temp data for performance
	private int[] xyz = new int[3];
	private int[] vTmp = new int[4];
	// persistent data
	private boolean loaded = false;
	// Opaque voxels
	private int vao, vbo, vaoSize;
	// Transparent voxels
	private int tvao, tvbo, tvaoSize;
	private long data;
	private LinearAlloc vertexBuffer;
	private boolean internalvBuffer;
	
	public int cx, cy, cz;
	
	private void init(LinearAlloc vertexBuffer) {
		internalvBuffer = false;
		if (vertexBuffer != null)
			this.vertexBuffer = vertexBuffer;
		else {
			this.vertexBuffer = new LinearAlloc(256);
			internalvBuffer = true;
		}
		data = nmemAlloc(CHUNK_FULL_SIZE);
		memSet(data, 0, CHUNK_FULL_SIZE);
		
		vbo = glGenBuffers();
		vao = glGenVertexArrays();
		tvao = glGenBuffers();
		tvbo = glGenVertexArrays();
	}
	public Chunk(LinearAlloc vertexBuffer) {
		init(vertexBuffer);
	}
	public Chunk(LinearAlloc vertexBuffer, int cx, int cy, int cz) {
		this.cx = cx;
		this.cy = cy;
		this.cz = cz;
		init(vertexBuffer);
	}
	
	//// Mesh building stuff
	private int encodeVertex(int texID, int uv, int luma) {
		// XXXX XXYY YYYY ZZZZ ZZUV LLLL TTTT TTTT
		// X, Y, Z are coordinates between 0 and 32
		// UV are texture coordinates
		// L is block luminosity between 0 and 15
		// T is the texture ID in the atlas
		return (xyz[0] << 26) | (xyz[1] << 20) | (xyz[2] << 14) | (uv << 12) | (luma << 8) | texID;
	}
	public void pushFace(int x, int y, int z, int texID, int luma, int dir) {
		// Gets the component, x is 0, y is 1 and z is 2
		int component = (dir & 6) << 1;
		// Does the axis component needs to be increased?
		int backface = dir & 1;
		// Position offsets for the quad
		int oa = FACE_OFFSETS[component], ob = FACE_OFFSETS[component + 1];
		// Does the face needs to be flipped?
		int oc = FACE_OFFSETS[component + 2] ^ backface;
		// offset of the UV_ORDER array accordingly to oc
		int uvOrdOffset = oc << 2;
		component >>= 2;
		
		// Adjust luma according to face direction
		luma = Math.max(0, luma - LUMA_SHADOW[component]);
		
		xyz[0] = x; xyz[1] = y; xyz[2] = z;
		xyz[component] += backface;
		
		// encode the 4 vertices to generate the quad
		vTmp[0]    = encodeVertex(texID, UV_ORDER[uvOrdOffset], luma); // b
		xyz[oa]++;
		vTmp[1+oc] = encodeVertex(texID, UV_ORDER[uvOrdOffset + 1], luma); // a
		xyz[ob]++;
		vTmp[3]    = encodeVertex(texID, UV_ORDER[uvOrdOffset + 2], luma); // a
		xyz[oa]--;
		vTmp[2-oc] = encodeVertex(texID, UV_ORDER[uvOrdOffset + 3], luma); // b
		
		// Push 2 triangles. A face then weights a total of 6 * 4 = 24 bytes
		long ptr = vertexBuffer.allocate(6);
		memPutInt(ptr, vTmp[0]);
		memPutInt(ptr + 4, vTmp[1]);
		memPutInt(ptr + 8, vTmp[2]);
		memPutInt(ptr + 12, vTmp[1]);
		memPutInt(ptr + 16, vTmp[3]);
		memPutInt(ptr + 20, vTmp[2]);
	}
	
	public void begin() {
		vertexBuffer.rewind();
	}
	public void commit() {
		vaoSize = commit(vbo, vao);
	}
	public void commitT() {
		tvaoSize = commit(tvbo, tvao);
	}
	private int commit(int vbo, int vao) {
		loaded = true;
		
		int vsize = vertexBuffer.realSize();
		
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		nglBufferData(GL_ARRAY_BUFFER, vsize, vertexBuffer.buffer(), GL_DYNAMIC_DRAW);
		
		glBindVertexArray(vao);
		glVertexAttribIPointer(0, 1, GL_UNSIGNED_INT, 4, 0);
		glEnableVertexAttribArray(0);
		
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		return vertexBuffer.size();
	}
	
	public void draw(Shader shader) {
		shader.use();
		shader.set("offset", cx, cy, cz);
		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLES, 0, vaoSize);
		glBindVertexArray(0);
	}
	public void drawT(Shader shader) {
		shader.use();
		shader.set("offset", cx, cy, cz);
		glBindVertexArray(tvao);
		glDrawArrays(GL_TRIANGLES, 0, tvaoSize);
		glBindVertexArray(0);
	}
	
	//// BLOCK STUFF
	public void setBlock(int x, int y, int z, int bid) {
		int i = (z + (y * CHUNK_SIZE)) * CHUNK_SIZE + x;
		memPutByte(data + i, (byte)bid);
	}
	public int getBlock(int x, int y, int z) {
		int i = (z + (y * CHUNK_SIZE)) * CHUNK_SIZE + x;
		return memGetByte(data + i);
	}
	
	public int cx() { return cx; }
	public int cy() { return cy; }
	public int cz() { return cz; }
	public long data() { return data; }
	public int data(int idx) { return memGetByte(data + idx); }
	public boolean loaded() { return loaded; }
	
	// Unload chunk from video memory
	public void unload() {
		loaded = false;
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, 0, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, tvbo);
		glBufferData(GL_ARRAY_BUFFER, 0, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	public void delete() {
		if (internalvBuffer)
			vertexBuffer.free();
		nmemFree(data);
		glDeleteVertexArrays(vao);
		glDeleteBuffers(vbo);
	}
}
