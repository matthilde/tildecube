package de.matthil.tildecube.world;

import de.matthil.glgraphics.*;

import static de.matthil.tildecube.world.Chunk.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.joml.FrustumIntersection;
import org.joml.Vector3f;

public class World {
	int x, y, z, cx, cy, cz;
	
	private ArrayList<Chunk> chunkRenderList = new ArrayList<Chunk>(256);
	
	private ArrayList<Chunk> chunkLoadList = new ArrayList<Chunk>(256);
	private ArrayList<Chunk> prevChunkLoadList = new ArrayList<Chunk>(256);
	
	private ArrayList<Chunk> chunkLoadQueue = new ArrayList<Chunk>(256);
	
	private int chunkRenderDistance = 2;
	
	private LinearAlloc vertexBuffer;
	private Chunk[] chunks;
	private ArrayList<Block> blocks;
	private Shader shader;
	private Texture atlas;
	
	public World(Shader shader, Texture atlas, ArrayList<Block> blocks, int cx, int cy, int cz) {
		this.blocks = blocks;
		this.shader = shader;
		this.atlas = atlas;
		
		vertexBuffer = new LinearAlloc(256);
		this.cx = cx;
		this.cy = cy;
		this.cz = cz;
		x = cx << 5;
		y = cy << 5;
		z = cz << 5;
		
		initChunks();
	}
	public void delete() {
		vertexBuffer.free();
		deleteChunks();
	}
	
	public ArrayList<Block> blocks() { return blocks; }
	public int x() { return x; }
	public int y() { return y; }
	public int z() { return z; }
	
	private boolean isTypeIncorrect(int bid, BlockType type) {
		return bid == 0 || blocks.get(bid - 1).translucent != type;
	}
	private boolean isTypeIncorrect(int x, int y, int z, BlockType type) {
		return isTypeIncorrect(ugetBlock(x, y, z), type);
	}
	
	public void updateOneChunkMesh(Chunk chunk, BlockType blockType, boolean updateShadow) {
		int cx = chunk.cx(), cy = chunk.cy(), cz = chunk.cz();
		int offX = cx << 5, offY = cy << 5, offZ = cz << 5;
		
		boolean isLeaves = blockType == BlockType.LEAVES;
		for (int i = CHUNK_FULL_SIZE - 1; i >= 0; --i) {
			int bid = chunk.data(i);
			if (isTypeIncorrect(bid, blockType)) continue;
			
			Block block = blocks.get(bid - 1);
			int x = (i % CHUNK_SIZE),
				z = ((i / CHUNK_SIZE) % CHUNK_SIZE),
				y = (i / (CHUNK_SIZE * CHUNK_SIZE)),
				ox = offX + x,
				oy = offY + y,
				oz = offZ + z;
			
			if (isLeaves) {
				chunk.pushFace(x, y, z, block.left, 15, 0);
				chunk.pushFace(x, y, z, block.right, 15, 1);
				chunk.pushFace(x, y, z, block.down, 15, 2);
				chunk.pushFace(x, y, z, block.up, 15, 3);
				chunk.pushFace(x, y, z, block.backward, 15, 4);
				chunk.pushFace(x, y, z, block.forward, 15, 5);
				continue;
			}
			
			if (ox <= 0 || isTypeIncorrect(ox-1, oy, oz, blockType))
				chunk.pushFace(x, y, z, block.left, 15, 0);
			if (ox >= this.x-1 || isTypeIncorrect(ox+1, oy, oz, blockType))
				chunk.pushFace(x, y, z, block.right, 15, 1);
			if (oy <= 0 || isTypeIncorrect(ox, oy-1, oz, blockType))
				chunk.pushFace(x, y, z, block.down, 15, 2);
			if (oy >= this.y-1 || isTypeIncorrect(ox, oy+1, oz, blockType))
				chunk.pushFace(x, y, z, block.up, 15, 3);
			if (oz <= 0 || isTypeIncorrect(ox, oy, oz-1, blockType))
				chunk.pushFace(x, y, z, block.backward, 15, 4);
			if (oz >= this.z-1 || isTypeIncorrect(ox, oy, oz+1, blockType))
				chunk.pushFace(x, y, z, block.forward, 15, 5);
		}
	}
	public void updateChunkMesh(int cx, int cy, int cz) {
		Chunk chunk = getChunk(cx, cy, cz);
		updateChunkMesh(chunk);
	}
	public void updateChunkMesh(int idx) {
		updateChunkMesh(chunks[idx]);
	}
	private void updateChunkMesh(Chunk chunk) {
		chunk.begin();
		updateOneChunkMesh(chunk, BlockType.SOLID, true);
		chunk.commit();
		chunk.begin();
		updateOneChunkMesh(chunk, BlockType.LEAVES, false);
		updateOneChunkMesh(chunk, BlockType.TRANSLUCENT, false);
		updateOneChunkMesh(chunk, BlockType.FLUID, false);
		chunk.commitT();
	}
	private void updateChunkMeshFromBlockChange(int x, int y, int z) {
		int cx = x >> 5, cy = y >> 5, cz = z >> 5;
		updateChunkMesh(cx, cy, cz);
		if (x > 0 && (x % CHUNK_SIZE) == 0)
			updateChunkMesh(cx - 1, cy, cz);
		else if (x < this.x - 1 && (x % CHUNK_SIZE) == CHUNK_SIZE - 1)
			updateChunkMesh(cx + 1, cy, cz);
		
		if (y > 0 && (y % CHUNK_SIZE) == 0)
			updateChunkMesh(cx, cy - 1, cz);
		else if (y < this.y - 1 && (y % CHUNK_SIZE) == CHUNK_SIZE - 1)
			updateChunkMesh(cx, cy + 1, cz);
		
		if (z > 0 && (z % CHUNK_SIZE) == 0)
			updateChunkMesh(cx, cy, cz - 1);
		else if (z < this.z - 1 && (z % CHUNK_SIZE) == CHUNK_SIZE - 1)
			updateChunkMesh(cx, cy, cz + 1);
	}
	public void updateMesh() {
		for (Chunk chunk : chunkRenderList)
			updateChunkMesh(chunk);
	}
	
	public void setChunkRenderDistance(int distance) {
		clearLoadLists();
		chunkRenderDistance = distance;
	}
	private void clearLoadLists() {
		chunkLoadList.clear();
		prevChunkLoadList.clear();
	}
	private void swapLoadLists() {
		ArrayList<Chunk> tmp = chunkLoadList;
		chunkLoadList = prevChunkLoadList;
		prevChunkLoadList = tmp;
		chunkLoadList.clear();
	}
	private void updateChunkLoadQueue() {
		chunkLoadQueue.clear();
		for (Chunk chunk : chunkLoadList) {
			if (!chunk.loaded())
				chunkLoadQueue.add(chunk);
		}
	}
	public void loadChunkFromQueue() {
		if (!chunkLoadQueue.isEmpty())
			updateChunkMesh(chunkLoadQueue.remove(chunkLoadQueue.size() - 1));
	}
	private void checkChunkForCulling(FrustumIntersection frustum, int x, int y, int z) {
		int cx = x * CHUNK_SIZE, cy = y * CHUNK_SIZE, cz = z * CHUNK_SIZE;
		if (x >= 0 && x < this.cx && y >= 0 && y < this.cy && z >= 0 && z < this.cz) {
			chunkLoadList.add(getChunk(x, y, z));
			if (frustum.testAab(cx, cy, cz, cx + 32, cy + 32, cz + 32))
				chunkRenderList.add(getChunk(x, y, z));
		}
	}
	private void unloadOldChunks() {
		for (Chunk chunk : prevChunkLoadList)
			if (!chunkLoadList.contains(chunk)) chunk.unload();
	}
	// Frustum culling
	public void cullChunks(Camera cam) {
		swapLoadLists();
		chunkRenderList.clear();
		FrustumIntersection frustum = cam.getFrustum();
		int x, y, z, dx, dz, tmp, size, curSize;
		size = 2 * chunkRenderDistance;
		// y = (int)cam.y() / 32 - chunkRenderDistance;
		
		// drawChunk(x, y);
		// for (int dy = y; dy < y + size; ++dy) {
		int dy = chunkRenderDistance;
		while (true) {
			x = (int)cam.x() / 32 - chunkRenderDistance;
			y = (int)cam.y() / 32 + dy;
			z = (int)cam.z() / 32 - chunkRenderDistance;
			dx = 1; dz = 0;
			curSize = size;
			
			checkChunkForCulling(frustum, x, y, z);
			while (curSize > 0) {
				for (int i = 0; i < (curSize == size ? 3 : 2); ++i) {
					for (int j = 0; j < curSize; ++j) {
						x += dx; z += dz;
						checkChunkForCulling(frustum, x, y, z);
					}
					tmp = dz;
					dz = dx;
					dx = -tmp;
				}
				curSize--;
			}
			
			dy = -dy;
			if (dy > 0) dy--;
			else if (dy == 0) break;
		}
		
		unloadOldChunks();
		if (!prevChunkLoadList.containsAll(chunkLoadList))
			updateChunkLoadQueue();
	}
	
	@Deprecated
	public void drawAll(Camera cam) {
		atlas.bind();
		shader.set("camera", cam.getCameraMatrix());
		for (Chunk chunk : chunks) {
			chunk.draw(shader);
			chunk.drawT(shader);
		}
		atlas.unbind();
	}
	public void draw(Camera cam) {
		cullChunks(cam);
		
		atlas.bind();
		shader.use();
		shader.set("camera", cam.getCameraMatrix());
		for (Chunk chunk : chunkRenderList) {
			// if (!chunk.loaded()) updateChunkMesh(chunk);
			if (chunk.loaded())
				chunk.draw(shader);
		}
		for (Chunk chunk : chunkRenderList)
			chunk.drawT(shader);
		atlas.unbind();
	}
	
	public void setBlock(int x, int y, int z, int bid) {
		if (x < 0 || x >= this.x || y < 0 || y >= this.y || z < 0 || z >= this.z)
			return;
		getChunkFromPos(x, y, z).setBlock(x&31, y&31, z&31, bid);
		updateChunkMeshFromBlockChange(x, y, z);
	}
	public int getBlock(int x, int y, int z) {
		if (x < 0 || x >= this.x || y < 0 || y >= this.y || z < 0 || z >= this.z)
			return 0;
		return getChunkFromPos(x, y, z).getBlock(x&31, y&31, z&31);
	}
	// Unsafe branchless variants
	public int ugetBlock(int x, int y, int z) {
		return getChunkFromPos(x, y, z).getBlock(x&31, y&31, z&31);
	}
	public void usetBlock(int x, int y, int z, int bid) {
		getChunkFromPos(x, y, z).setBlock(x&31, y&31, z&31, bid);
	}
	
	public void resize(int cx, int cy, int cz) {
		this.cx = cx;
		this.cy = cy;
		this.cz = cz;
		x = cx << 5;
		y = cy << 5;
		z = cz << 5;
		
		deleteChunks();
		initChunks();
	}
	
	@Deprecated
	public void save(File file, Vector3f playerPosition) throws IOException {
		DataOutputStream f = new DataOutputStream(new FileOutputStream(file));
		
		f.writeInt(x);
		f.writeInt(y);
		f.writeInt(z);
		f.writeInt((int)playerPosition.x());
		f.writeInt((int)playerPosition.y());
		f.writeInt((int)playerPosition.z());
		
		int count = 0; byte[] buffer = new byte[512];
		for (int i = 0; i < x*y*z; ++i) {
			if (count >= buffer.length) {
				f.write(buffer);
				count = 0;
			}
			
			int ix = i % x,
				iy = i / (x * z),
				iz = (i / x) % z;
			buffer[count] = (byte)getBlock(ix, iy, iz);
			
			++count;
		}
		f.write(buffer, 0, count);
		
		f.close();
	}
	
	@Deprecated
	public void load(File file, Vector3f playerPosition) throws IOException {
		DataInputStream f = new DataInputStream(new FileInputStream(file));
		
		int x = f.readInt();
		int y = f.readInt();
		int z = f.readInt();
		playerPosition.setComponent(0, f.readInt() + .5f);
		playerPosition.setComponent(1, f.readInt());
		playerPosition.setComponent(2, f.readInt() + .5f);
		
		resize((int)Math.ceil((double)x / 32), (int)Math.ceil((double)y / 32), (int)Math.ceil((double)z / 32));
		int sz, count = 0; byte[] buffer = new byte[512];
		while (count < x*y*z) {
			if ((sz = f.read(buffer)) < 0)
				break;
			for (int i = 0; i < sz; ++i) {
				int ix = (i + count) % x,
					iy = (i + count) / (x * z),
					iz = ((i + count) / x) % z;
				
				usetBlock(ix, iy, iz, buffer[i]);
			}
			count += sz;
		}
		
		f.close();
	}
	
	public Chunk getChunk(int x, int y, int z) {
		return chunks[x+y*cx*cz+z*cx];
	}
	public int getChunkIdx(int x, int y, int z) {
		return x+y*cx*cz+z*cx;
	}
	private Chunk getChunkFromPos(int x, int y, int z) {
		x >>= 5; y >>= 5; z >>= 5;
		return chunks[x+y*cx*cz+z*cx];
	}
	private void initChunks() {
		chunks = new Chunk[cx*cy*cz];
		for (int i = 0; i < cx*cy*cz; ++i)
			chunks[i] = new Chunk(vertexBuffer, i%cx, i/(cx*cz), (i/cx)%cz);
	}
	private void deleteChunks() {
		for (Chunk chunk : chunks)
			chunk.delete();
	}
}
