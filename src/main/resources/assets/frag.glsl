#version 330 core
in vec2 texCoords;
in vec3 normal;
in float luma;

uniform sampler2D tex;

uniform vec3 fogColor;
uniform float fogEnd;

out vec4 outputColor;

void main() {
	// outputColor = vec4(1.0, 0.0, 1.0, 1.0);
	// outputColor = vec4(color, 1);
	vec4 texColor = texture(tex, texCoords);
	if (texColor.a < 0.1) discard;

	float depth = 1.0 / gl_FragCoord.w;
	float f = clamp((fogEnd - depth) / fogEnd, 0.0, 1.0);
	// float f = clamp(exp(fogEnd * depth), 0.0, 1.0);

	float l = 1.0 - abs(normal.x) * 0.1 - abs(normal.z) * 0.2;
	vec4 ocolor = vec4(texColor.rgb * l * luma, texColor.a);

	ocolor.rgb = mix(fogColor, ocolor.rgb, f);
	outputColor = ocolor;
}
