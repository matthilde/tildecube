#version 330 core
in vec2 texCoords;

uniform sampler2D tex;
uniform vec4 color;

out vec4 outputColor;

void main() {
	vec4 texColor = vec4(0);
	if (color.a < 0.01)
		texColor = texture(tex, texCoords);
	else
		texColor = color;

	outputColor = texColor;
}
