#version 330 core
layout (location = 0) in vec3 vertex;
layout (location = 2) in vec2 uv;

uniform mat4 projection;
// uniform mat4 transform;

// xy -> position; zw -> size
uniform vec4 posSize;
// xy -> position; zw -> size
uniform vec4 uvOffset;

out vec2 texCoords;

void main() {
	texCoords = uv * uvOffset.zw + uvOffset.xy;
	// gl_Position = projection vec4(vertex.xy + position, 0, 1);
	vec2 vert = vertex.xy * posSize.zw;
	gl_Position = projection * vec4(vert + posSize.xy, 0, 1);
}
