#version 330 core
layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 uv;
layout (location = 3) in float vLuma;

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 transform;

out vec2 texCoords;
out vec3 normal;
out float luma;

void main() {
	texCoords = uv;
	normal = vNormal;
	luma = vLuma;
	gl_Position = perspective * camera * transform * vec4(vertex, 1.0);
	// gl_Position = vec4(vertex, 1.0);
}
