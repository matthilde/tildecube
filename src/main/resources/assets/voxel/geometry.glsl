layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in VS_OUT {
    vec2  texCoords;
    vec3  normal;
    float luma;
    uint  faceID;
} gs_in[];

out vec2 texCoords;
out vec3 normal;
out float luma;

uniform mat4 perspective;
uniform mat4 camera;

void upperFace(vec4 position) {
    vec2 tex = gs_in[0].texCoords;
    vec3 nOffset = normal * 0.5;
    normal    = gs_in[0].normal;
    luma      = gs_in[0].luma;
    texCoords = tex;
    
    gl_Position = position + nOffset + vec4(-0.5, 0, -0.5, 0);
    EmitVertex();
    gl_Position = position + nOffset + vec4( 0.5, 0, -0.5, 0);
    texCoords = tex + vec2(0.0625, 0);
    EmitVertex();
    gl_Position = position + nOffset + vec4(-0.5, 0,  0.5, 0);
    texCoords = tex + vec2(0, 0.0625);
    EmitVertex();
    gl_Position = position + nOffset + vec4( 0.5, 0,  0.5, 0);
    texCoords = tex + vec2(0.0625, 0.0625);
    EmitVertex();
    EndPrimitive();
}

void main() {
    vec4 position = gs_in[0].gl_Position;
    switch (faceID) {
    case 2: upperFace(position); break;
    }
}