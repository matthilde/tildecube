#version 330 core

// XXXX XXYY YYYY ZZZZ ZZUV LLLL TTTT TTTT
// XYZ - coords
// UV  - uv offset
// L - luminosity
// T - Texture ID
layout (location = 0) in uint voxelInfo;

uniform mat4 perspective;
uniform mat4 camera;
// uniform mat4 transform;
uniform ivec3 offset;

out vec2 texCoords;
out vec3 normal;
out float luma;

const vec2 uvOffsets[4] = vec2[4](
    vec2(0.0, 0.0),
    vec2(0.0, 1.0),
    vec2(1.0, 0.0),
    vec2(1.0, 1.0)
);

void main() {
    vec3 position = vec3(
        (voxelInfo >> 26),
        (voxelInfo >> 20) & 63U,
        (voxelInfo >> 14) & 63U
    ) + vec3(offset << 5);
    uint uv = (voxelInfo >> 12U) & 3U;
    uint texID = voxelInfo & 255U;
    
    // 1 / 16 = 0.0625
    texCoords = (vec2(texID & 15U, texID >> 4U) + uvOffsets[uv]) * 0.0625;
    normal    = vec3(0);
    luma      = float((voxelInfo >> 8U) & 15U) / 16;
    
    gl_Position = perspective * camera * vec4(position, 1.0);
}
